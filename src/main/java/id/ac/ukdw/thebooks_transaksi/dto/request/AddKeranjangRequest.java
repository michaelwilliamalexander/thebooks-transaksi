package id.ac.ukdw.thebooks_transaksi.dto.request;

import lombok.Data;

@Data
public class AddKeranjangRequest {
    
    private String userEmail;
    
    private String isbn;

    public AddKeranjangRequest() {};

    public AddKeranjangRequest(String userEmail,String isbn){
        this.isbn = isbn;
        this.userEmail = userEmail;
    }
}
