Feature: Menambahkan Order

  Scenario Outline: Menambahkan data order dan Berhasil
    Given Terdapat detail alamat user
      |idAlamat|alamat               |kota               |provinsi         |kodePos          |emailUser                       |
      |1       |Jalan Merdeka no 31  |Yogyakarta         |DIY              |55152            |michael.william@ti.ukdw.ac.id   |
      |2       |Jalan Kebun Jeruk 2  |Jakarta            |DKI Jakarta      |11010            |michael.william@ti.ukdw.ac.id   |
      |3       |Jalan Senopati 23    |Yogyakarta         |DIY              |55132            |nathaniel.alvin@ti.ukdw.ac.id   |
    When user dengan email <emailUser> menambahkan data Order dengan request <idAlamat> , <items> , <qty> , <timeOrder> , <hargaItems>
    Then user mendapatkan Id Invoice dari penambahan Order dengan data request <emailUser> , <idAlamat> , <items> , <qty> , <timeOrder> , <hargaItems>
    Examples:
    |emailUser                      |idAlamat |items                         |qty    |timeOrder           |hargaItems     |
    |michael.william@ti.ukdw.ac.id  |1        |9781974710041, 9780804171588  |1,1    |2021-08-03 18:00:00 |100000, 115000 |
    |michael.william@ti.ukdw.ac.id  |1        |9781338732870                 |2      |2021-08-07 08:00:00 |200000         |
    |nathaniel.alvin@ti.ukdw.ac.id  |3        |9781974710041                 |1      |2021-08-03 18:00:00 |100000         |


  Scenario Outline: Menambahkan data Order dan Gagal
    Given Terdapat data-data alamat user
      |idAlamat|alamat               |kota               |provinsi         |kodePos          |emailUser                       |
      |1       |Jalan Merdeka no 31  |Yogyakarta         |DIY              |55152            |michael.william@ti.ukdw.ac.id   |
      |2       |Jalan Kebun Jeruk 2  |Jakarta            |DKI Jakarta      |11010            |michael.william@ti.ukdw.ac.id   |
      |3       |Jalan Senopati 23    |Yogyakarta         |DIY              |55132            |nathaniel.alvin@ti.ukdw.ac.id   |
    When user dengan email <emailUser> menambahkan Order dengan request <idAlamat> , <items> , <qty> , <timeOrder> , <hargaItems>
    Then user mendapatkan Bad Request dengan menambahkan Order dengan data request <emailUser> , <idAlamat> ,  <items> , <qty> , <timeOrder> , <hargaItems>
    Examples:
      |emailUser                      |idAlamat |items                         |qty    |timeOrder           |hargaItems     |
      |michael.william@ti.ukdw.ac.id  |3        |9781974710041, 9780804171588  |1,1    |2021-08-03 18:00:00 |100000, 115000 |
      |                               |1        |9781338732870                 |2      |2021-08-03 18:00:00 |200000         |
      |nathaniel.alvin@ti.ukdw.ac.id  |1        |                              |       |2021-08-03 18:00:00 |               |
      |                               |         |                              |       |                    |               |