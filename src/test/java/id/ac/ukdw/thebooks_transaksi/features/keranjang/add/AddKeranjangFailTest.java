package id.ac.ukdw.thebooks_transaksi.features.keranjang.add;

import id.ac.ukdw.thebooks_transaksi.config.ObjectMapping;
import id.ac.ukdw.thebooks_transaksi.controller.KeranjangController;
import id.ac.ukdw.thebooks_transaksi.dto.request.keranjang.KeranjangRequest;
import id.ac.ukdw.thebooks_transaksi.exception.BadRequestException;
import id.ac.ukdw.thebooks_transaksi.model.TableKeranjang;
import id.ac.ukdw.thebooks_transaksi.model.DetailKeranjang;
import id.ac.ukdw.thebooks_transaksi.model.Keranjang;
import id.ac.ukdw.thebooks_transaksi.service.KeranjangService;
import io.cucumber.java.Before;
import io.cucumber.java.en.*;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class AddKeranjangFailTest {

    @InjectMocks
    private KeranjangController controller;

    @Mock
    private KeranjangService service;

    private MockMvc mockMvc;

    private Keranjang dataKeranjang;

    private List<DetailKeranjang> dataDetail;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
        dataDetail = new ArrayList<>();
    }

    @Given("^Terdapat data=data keranjang$")
    public void data(List<TableKeranjang> data) {
        for (TableKeranjang item:data){
            dataKeranjang = new Keranjang(item.getIdKeranjang(),item.getEmailUser());
            List<String> isbn = Arrays.asList(item.getItems().split("\\s*,\\s*"));
            List<String> qty = Arrays.asList(item.getQty().split("\\s*,\\s*"));
            if (isbn.size() == qty.size()){
                for (int i=0;i<isbn.size();i++){
                    dataDetail.add(new DetailKeranjang(dataKeranjang,isbn.get(i),Integer.parseInt(qty.get(i))));
                }
            }
        }
    }

    @When("^user dengan email (.*) akan menambahkan item keranjang dengan buku berisbn (.*)$")
    public void input(final String email,
                      final String isbn) {
        when(service.addKeranjang(isbn,email)).thenThrow(new BadRequestException());
    }
    @Then("^user mendapatkan Bad Request dari penambahan keranjang denga request (.*) , (.*)")
    public void output(final String email,
                       final String isbn) throws Exception
    {
        mockMvc.perform(MockMvcRequestBuilders.post("/order/keranjang/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(ObjectMapping.asJsonString(new KeranjangRequest(isbn,email))))
                .andExpect(status().isBadRequest())
                .andDo(print());

        verify(service).addKeranjang(isbn,email);
    }
}
