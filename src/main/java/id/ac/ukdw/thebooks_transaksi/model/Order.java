package id.ac.ukdw.thebooks_transaksi.model;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Entity
@Data
@Table(name = "data_order")
public class Order {

    @Id
    @NotEmpty @NotNull
    @Column(name = "id_order")
    private String idOrder;

    @NotNull @NotEmpty
    @Column(name = "email_user")
    private String emailUser;

    @NotEmpty @NotNull
    @Column(name = "alamat")
    private String alamat;

    @NotNull @NotEmpty
    @Column(name = "time_order")
    private String timeOrder;

    @NotEmpty @NotNull
    @Column(name = "status_order",columnDefinition="tinyint")
    private boolean statusOrder;

    @NotEmpty @NotNull
    @Column(name = "harga_total")
    private double totalHarga;

    public Order(){}

    public Order(String idOrder,
                 String alamat,
                 String emailUser,
                 String timeOrder,
                 boolean statusOrder,
                 double totalHarga){

        this.idOrder = idOrder;
        this.alamat = alamat;
        this.emailUser = emailUser;
        this.timeOrder = timeOrder;
        this.statusOrder = statusOrder;
        this.totalHarga = totalHarga;
    }

    public Order(String alamat,
                 String emailUser,
                 String timeOrder){
        this.idOrder = "INV/"+ UUID.randomUUID();
        this.alamat = alamat;
        this.emailUser = emailUser;
        this.timeOrder = timeOrder;
        this.statusOrder = false;
    }
}
