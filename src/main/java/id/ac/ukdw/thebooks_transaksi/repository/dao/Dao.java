package id.ac.ukdw.thebooks_transaksi.repository.dao;

import java.util.Optional;

public interface Dao <T,ID>{

    Optional<T> findById(ID id);

    boolean save(T t);

    boolean delete(ID id);

    boolean update(T t);

}
