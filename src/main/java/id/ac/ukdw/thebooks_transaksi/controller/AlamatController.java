package id.ac.ukdw.thebooks_transaksi.controller;

import id.ac.ukdw.thebooks_transaksi.dto.request.alamat.AlamatRequest;
import id.ac.ukdw.thebooks_transaksi.dto.request.alamat.UpdateAlamatRequest;
import id.ac.ukdw.thebooks_transaksi.dto.response.ResponseWrapper;
import id.ac.ukdw.thebooks_transaksi.service.AlamatService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import static id.ac.ukdw.thebooks_transaksi.config.SwaggerConfig.*;

@Controller
@RequestMapping("/order/alamat")
@Api(tags = alamatServiceTag)
@RequiredArgsConstructor
public class AlamatController {

    private final AlamatService service;

    @PostMapping(value = "/",produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Menambahkan data alamat ")
    public ResponseEntity<ResponseWrapper> saveAlamat(@RequestBody AlamatRequest request){
        return ResponseEntity.ok(new ResponseWrapper(
                service.saveAlamat(
                        request.getAlamat(),
                        request.getKota(),
                        request.getProvinsi(),
                        request.getKodePos(),
                        request.getEmailUser())
        ));
    }

    @DeleteMapping(value= "/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Menghapus data alamat")
    public ResponseEntity<ResponseWrapper> deleteAlamat(@PathVariable("id") int idAlamat){
        return ResponseEntity.ok(new ResponseWrapper(
                service.deleteAlamat(idAlamat)
        ));
    }

    @GetMapping(value = "/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Mengambil detail alamat ")
    public ResponseEntity<ResponseWrapper> getDetailAlamat(@PathVariable("id") int idAlamat){
        return ResponseEntity.ok(new ResponseWrapper(
                service.getAlamat(idAlamat)
        ));
    }

    @GetMapping(value = "/",produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Mengambil Semua data Alamat")
    public ResponseEntity<ResponseWrapper> getAllByEmail(@RequestParam("email") String email){
        return ResponseEntity.ok(new ResponseWrapper(
                service.getAllAlamatUser(email)
        ));
    }

    @PutMapping(value = "/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Memperbarui data alamat")
    public ResponseEntity<ResponseWrapper> updateAlamat(@PathVariable("id") int idAlamat,
                                                        @RequestBody UpdateAlamatRequest request){
        return ResponseEntity.ok(new ResponseWrapper(
                service.updateAlamat(
                        idAlamat,
                        request.getAlamat(),
                        request.getKota(),
                        request.getProvinsi(),
                        request.getKodePos()
                )
        ));
    }
}
