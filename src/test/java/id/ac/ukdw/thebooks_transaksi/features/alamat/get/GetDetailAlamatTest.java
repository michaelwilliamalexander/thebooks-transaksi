package id.ac.ukdw.thebooks_transaksi.features.alamat.get;

import id.ac.ukdw.thebooks_transaksi.controller.AlamatController;
import id.ac.ukdw.thebooks_transaksi.dto.AlamatDto;
import id.ac.ukdw.thebooks_transaksi.model.Alamat;
import id.ac.ukdw.thebooks_transaksi.service.AlamatService;
import io.cucumber.java.Before;
import io.cucumber.java.en.*;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class GetDetailAlamatTest {

    @InjectMocks
    private AlamatController controller;

    @Mock
    private AlamatService service;

    @Autowired
    private ModelMapper mapper;

    private MockMvc mockMvc;

    private List<Alamat> dataAlamat;

    private AlamatDto detailAlamat;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
        dataAlamat = new ArrayList<>();
    }

    @Given("^Data List Alamat$")
    public void data(List<Alamat> data) {
        dataAlamat = data;
    }

    @When("^user ingin mendapatkan detail email dengan idAlamat (.*)$")
    public void input(final int idAlamat) {
        for (Alamat item:dataAlamat){
            if (item.getIdAlamat()== idAlamat){
                detailAlamat = mapper.map(item,AlamatDto.class);
                when(service.getAlamat(idAlamat)).thenReturn(detailAlamat);
            }
        }
    }
    @Then("^user mendapatkan detail alamat dari request id (.*)$")
    public void output(final int idAlamat) throws Exception{

        mockMvc.perform(MockMvcRequestBuilders.get("/order/alamat/{id}",idAlamat)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.data.idAlamat",is(detailAlamat.getIdAlamat())))
                .andExpect(jsonPath("$.data.alamat",is(detailAlamat.getAlamat())))
                .andExpect(jsonPath("$.data.kota",is(detailAlamat.getKota())))
                .andExpect(jsonPath("$.data.provinsi",is(detailAlamat.getProvinsi())))
                .andExpect(jsonPath("$.data.kodePos",is(detailAlamat.getKodePos())))
                .andExpect(jsonPath("$.data.emailUser",is(detailAlamat.getEmailUser())));

        verify(service).getAlamat(idAlamat);
    }
}
