package id.ac.ukdw.thebooks_transaksi.model;

import lombok.Data;

import java.util.List;

@Data
public class TableKeranjang {

    String idKeranjang;

    String emailUser;

    String items;

    String qty;
}
