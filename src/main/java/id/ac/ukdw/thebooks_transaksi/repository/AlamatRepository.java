package id.ac.ukdw.thebooks_transaksi.repository;

import id.ac.ukdw.thebooks_transaksi.model.Alamat;
import id.ac.ukdw.thebooks_transaksi.repository.dao.AlamatDao;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
@Transactional
@RequiredArgsConstructor
public class AlamatRepository implements AlamatDao {

    private final EntityManager entityManager;

    @Override
    public Optional<Alamat> findById(Integer id) {
        String hql = "select alm from Alamat alm where alm.idAlamat = :idAlamat";
        Query query = entityManager.createQuery(hql);
        query.setParameter("idAlamat",id);
        List<Alamat> data = query.getResultList();
        Alamat alamat = null;
        for (Alamat item: data){
            alamat = item;
        }
        return Optional.ofNullable(alamat);
    }

    @Override
    public boolean save(Alamat alamat) {
        String hql = "insert into alamat(alamat,kota,provinsi,kode_pos,email_user) values(?,?,?,?,?)";
        Query query = entityManager.createNativeQuery(hql);
        query.setParameter(1,alamat.getAlamat());
        query.setParameter(2,alamat.getKota());
        query.setParameter(3,alamat.getProvinsi());
        query.setParameter(4,alamat.getKodePos());
        query.setParameter(5,alamat.getEmailUser());
        query.executeUpdate();
        return true;
    }

    @Override
    public boolean delete(Integer id) {
        String hql = "delete from Alamat alm where alm.idAlamat = :id";
        Query query = entityManager.createQuery(hql);
        query.setParameter("id",id);
        query.executeUpdate();
        return true;
    }

    @Override
    public boolean update(Alamat alamat) {
        entityManager.merge(alamat);
        return true;
    }

    @Override
    public Optional<List<Alamat>> findByEmailUser(String email) {
        String hql = "select alm from Alamat alm where alm.emailUser = :email";
        Query query = entityManager.createQuery(hql);
        query.setParameter("email",email);
        List<Alamat> data = query.getResultList();
        return Optional.ofNullable(data);
    }
}
