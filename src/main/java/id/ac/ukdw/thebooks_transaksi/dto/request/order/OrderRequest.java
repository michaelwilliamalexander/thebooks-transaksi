package id.ac.ukdw.thebooks_transaksi.dto.request.order;

import id.ac.ukdw.thebooks_transaksi.model.OrderBuku;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class OrderRequest {

    private List<OrderBuku> buku;

    private String email;

    private Integer idAlamat;

    private String timeOrder;

    public OrderRequest(){}

    public OrderRequest(List<OrderBuku> buku,
                        String email,
                        Integer idAlamat,
                        String timeOrder){
        this.buku = buku;
        this.email = email;
        this.idAlamat = idAlamat;
        this.timeOrder = timeOrder;
    }
}
