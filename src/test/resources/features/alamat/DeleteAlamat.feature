Feature: Menghapus Alamat User

  Scenario Outline: Menghapus data Alamat dan Berhasil
    Given Terlist alamat user
      |idAlamat|alamat               |kota               |provinsi         |kodePos          |emailUser                       |
      |1       |Jalan Merdeka no 31  |Yogyakarta         |DIY              |55152            |michael.william@ti.ukdw.ac.id   |
      |2       |Jalan Kebun Jeruk 2  |Jakarta            |DKI Jakarta      |11010            |michael.william@ti.ukdw.ac.id   |
      |3       |Jalan Senopati 23    |Yogyakarta         |DIY              |55132            |nathaniel.alvin@ti.ukdw.ac.id   |
    When user ingin menghapus data alamat dengan id <idAlamat>
    Then  user mendapatkan pesan <pesan> dari menghapus alamat dengan id <idAlamat>
    Examples:
      |idAlamat |pesan              |
      |1        |Delete Berhasil    |
      |2        |Delete Berhasil    |

  Scenario Outline: Menghapus data Alamat dan Gagal
    Given Terlist user alamat
      |idAlamat|alamat               |kota               |provinsi         |kodePos          |emailUser                       |
      |1       |Jalan Merdeka no 31  |Yogyakarta         |DIY              |55152            |michael.william@ti.ukdw.ac.id   |
      |2       |Jalan Kebun Jeruk 2  |Jakarta            |DKI Jakarta      |11010            |michael.william@ti.ukdw.ac.id   |
      |3       |Jalan Senopati 23    |Yogyakarta         |DIY              |55132            |nathaniel.alvin@ti.ukdw.ac.id   |
    When user ingin menghapus data alamat berid <idAlamat>
    Then  user mendapatkan Bad Request dari menghapus alamat dengan id <idAlamat>
    Examples:
      |idAlamat |
      |4        |
      |5        |
