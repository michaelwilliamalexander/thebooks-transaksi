package id.ac.ukdw.thebooks_transaksi.dto;

import lombok.Data;

@Data
public class AlamatDto {

    private int idAlamat;

    private String alamat;

    private String kota;

    private String provinsi;

    private String kodePos;

    private String emailUser;

    public AlamatDto(){}

    public AlamatDto(int idAlamat,
                     String alamat,
                     String kota,
                     String provinsi,
                     String kodePos,
                     String emailUser){
        this.idAlamat = idAlamat;
        this.alamat = alamat;
        this.kota = kota;
        this.provinsi = provinsi;
        this.kodePos = kodePos;
        this.emailUser = emailUser;
    }
}
