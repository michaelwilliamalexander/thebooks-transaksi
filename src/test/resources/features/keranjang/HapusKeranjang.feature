Feature: Menghapus data keranjang

  Scenario Outline: Menghapus data item keranjang dan Berhasil
    Given list data Keranjang
      |emailUser                      |idKeranjang      |items                          |qty    |
      |nathaniel.alvin@ti.ukdw.ac.id  |aB132Qw          |9781974710041 , 9780804171588  |1, 2   |
      |michael.william@ti.ukdw.ac.id  |qe234QW          |9781338732870                  |1      |
    When user akan menghapus data keranjang dengan id <idKeranjang> dengan isbn <isbn>
    Then user mendapatkan pesan <pesan> dari penghapusan data keranjang <idKeranjang> , <isbn>
    Examples:
      |idKeranjang  |isbn             |pesan              |
      |aB132Qw      |9781974710041    |Keranjang dihapus  |
      |qe234QW      |9781338732870    |Keranjang dihapus  |

  Scenario Outline: Menghapus data item keranjang dan Gagal
    Given list data-data Keranjang
      |emailUser                      |idKeranjang      |items                          |qty    |
      |nathaniel.alvin@ti.ukdw.ac.id  |aB132Qw          |9781974710041 , 9780804171588  |1, 2   |
      |michael.william@ti.ukdw.ac.id  |qe234QW          |9781338732870                  |1      |
    When user menghapus data keranjang dengan id <idKeranjang> dengan isbn <isbn>
    Then user mendapatkan Bad Request ketika penghapusan data keranjang <idKeranjang> , <isbn>
    Examples:
      |idKeranjang  |isbn             |
      |qe234QW      |9781974710041    |
      |aB132Qw      |                 |

  Scenario Outline: Menghapus Semua data keranjang dan Berhasil
    Given data list keranjang
      |emailUser                      |idKeranjang      |items                          |qty    |
      |nathaniel.alvin@ti.ukdw.ac.id  |aB132Qw          |9781974710041 , 9780804171588  |1, 2   |
      |michael.william@ti.ukdw.ac.id  |qe234QW          |9781338732870                  |1      |
    When user ingin menghapus semua data keranjang dengan id <idKeranjang>
    Then user mendapatkan pesan <pesan> dari penghapusan keranjang dengan request <idKeranjang>
    Examples:
      |idKeranjang    |pesan              |
      |aB132Qw        |Keranjang terhapus |
      |qe234QW        |Keranjang terhapus |

  Scenario Outline:Menghapus semua data keranjang dan gagal
    Given data-data list keranjang
      |emailUser                      |idKeranjang      |items                          |qty    |
      |nathaniel.alvin@ti.ukdw.ac.id  |aB132Qw          |9781974710041 , 9780804171588  |1, 2   |
      |michael.william@ti.ukdw.ac.id  |qe234QW          |9781338732870                  |1      |
    When user menghapus semua data keranjang dengan id <idKeranjang>
    Then user mendapatkan Bad Request dari penghapusan keranjang dengan data request <idKeranjang>
    Examples:
      |idKeranjang    |
      |abcd123        |