package id.ac.ukdw.thebooks_transaksi.service;

import id.ac.ukdw.thebooks_transaksi.dto.AlamatDto;
import id.ac.ukdw.thebooks_transaksi.exception.BadRequestException;
import id.ac.ukdw.thebooks_transaksi.exception.NotFoundException;
import id.ac.ukdw.thebooks_transaksi.model.Alamat;
import id.ac.ukdw.thebooks_transaksi.repository.dao.AlamatDao;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.valid4j.Validation.validate;

@Service
@RequiredArgsConstructor
public class AlamatService {

    private final ModelMapper mapper;

    private final AlamatDao alamatRepository;

    public AlamatDto getAlamat(int idAlamat){
        Optional<Alamat> find  =  alamatRepository.findById(idAlamat);
        if(find.isPresent()){
            AlamatDto resultData = mapper.map(find.get(),AlamatDto.class);
            return resultData;
        }
        throw new NotFoundException();
    }

    public List<AlamatDto> getAllAlamatUser(String email){
        Optional<List<Alamat>> findData = alamatRepository.findByEmailUser(email);
        if (findData.isPresent()){
            List<AlamatDto> resultdata = new ArrayList<>();
            for (Alamat alamat: findData.get()){
                resultdata.add(mapper.map(alamat,AlamatDto.class));
            }
            return resultdata;
        }
        throw new NotFoundException();
    }

    public String saveAlamat(String alamat,
                             String kota,
                             String provinsi,
                             String kodePos,
                             String emailUser){
        validate(!emailUser.isEmpty(), new BadRequestException());
        validate(emailUser!=null, new BadRequestException());
        validate(!alamat.isEmpty(), new BadRequestException());
        validate(alamat !=null, new BadRequestException());
        validate(!kota.isEmpty(), new BadRequestException());
        validate(kota !=null, new BadRequestException());
        validate(!provinsi.isEmpty(),new BadRequestException());
        validate(provinsi !=null, new BadRequestException());
        validate(kodePos!=null, new BadRequestException());
        validate(!kodePos.isEmpty(), new BadRequestException());

        Alamat data = new Alamat(alamat,kota,provinsi,kodePos,emailUser);
        if(alamatRepository.save(data)){
            return "save berhasil";
        }
        throw new BadRequestException();
    }

    public String deleteAlamat(int idAlamat){
        Optional<Alamat> find = alamatRepository.findById(idAlamat);
        if(find.isPresent()) if(alamatRepository.delete(idAlamat)) {
            return "delete berhasil";
        }
        throw new BadRequestException();
    }

    public String updateAlamat(int idAlamat,
                               String alamat,
                               String kota,
                               String provinsi,
                               String kodePos){
        validate(!alamat.isEmpty(), new BadRequestException());
        validate(alamat !=null, new BadRequestException());
        validate(!kota.isEmpty(), new BadRequestException());
        validate(kota !=null, new BadRequestException());
        validate(!provinsi.isEmpty(),new BadRequestException());
        validate(provinsi !=null, new BadRequestException());
        validate(kodePos!=null, new BadRequestException());
        validate(!kodePos.isEmpty(), new BadRequestException());

        Optional<Alamat> find = alamatRepository.findById(idAlamat);
        if(find.isPresent()){
            Alamat data = new Alamat(idAlamat,alamat,kota,provinsi,kodePos,find.get().getEmailUser());
            if(alamatRepository.update(data)) {
                return "update berhasil";
            }
        }
        throw new BadRequestException();
    }
}
