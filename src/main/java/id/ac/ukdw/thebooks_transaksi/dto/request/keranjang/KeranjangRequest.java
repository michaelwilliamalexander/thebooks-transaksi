package id.ac.ukdw.thebooks_transaksi.dto.request.keranjang;

import id.ac.ukdw.thebooks_transaksi.model.Buku;
import lombok.Data;

@Data
public class KeranjangRequest {

    private String isbn;

    private String emailUser;

    public KeranjangRequest(){}

    public KeranjangRequest(String isbn,String email){
        this.emailUser = email;
        this.isbn = isbn;
    }

}
