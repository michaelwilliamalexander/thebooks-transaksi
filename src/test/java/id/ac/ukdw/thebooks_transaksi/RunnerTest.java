package id.ac.ukdw.thebooks_transaksi;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;

@RunWith(Cucumber.class)
@CucumberOptions(plugin = "pretty", features = "src/test/resources/features" )
@SpringBootTest(classes = {ThebooksTransaksiApplication.class
        ,RunnerTest.class})
public class RunnerTest {

}
