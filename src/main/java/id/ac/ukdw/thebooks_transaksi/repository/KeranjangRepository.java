package id.ac.ukdw.thebooks_transaksi.repository;

import id.ac.ukdw.thebooks_transaksi.model.Keranjang;
import id.ac.ukdw.thebooks_transaksi.repository.dao.KeranjangDao;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
@Transactional
@RequiredArgsConstructor
public class KeranjangRepository implements KeranjangDao{

    private final EntityManager entityManager;

    @Override
    public Optional<Keranjang> findById(String id) {
        String hql = "select  inv from Keranjang inv where inv.idKeranjang = :id";
        Query query = entityManager.createQuery(hql);
        query.setParameter("id",id);
        List<Keranjang> data = query.getResultList();
        Keranjang result = null;
        for (Keranjang keranjang:data){
            result = keranjang;
        }
        return Optional.ofNullable(result);
    }

    @Override
    public Optional<Keranjang> findByEmail(String email) {
        String hql = "select  inv from Keranjang inv where inv.emailUser = :email";
        Query query = entityManager.createQuery(hql);
        query.setParameter("email",email);
        List<Keranjang> data = query.getResultList();
        Keranjang result = null;
        for (Keranjang keranjang:data){
            result = keranjang;
        }
        return Optional.ofNullable(result);
    }

    @Override
    public boolean save(Keranjang keranjang) {
        String hql = "insert into keranjang(id_keranjang,email_user) values (?,?)";
        Query query = entityManager.createNativeQuery(hql);
        query.setParameter(1, keranjang.getIdKeranjang());
        query.setParameter(2, keranjang.getEmailUser());
        query.executeUpdate();
        return true;
    }

    @Override
    public boolean delete(String id) {
        String hql = "delete from Keranjang inv where inv.idKeranjang = :id";
        Query query = entityManager.createQuery(hql);
        query.setParameter("id",id);
        query.executeUpdate();
        return true;
    }

    @Override
    public boolean update(Keranjang keranjang) {
        entityManager.merge(keranjang);
        return true;
    }



}
