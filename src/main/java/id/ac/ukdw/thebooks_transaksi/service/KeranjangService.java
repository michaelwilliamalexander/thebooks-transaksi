package id.ac.ukdw.thebooks_transaksi.service;

import id.ac.ukdw.thebooks_transaksi.dto.KeranjangDto;
import id.ac.ukdw.thebooks_transaksi.exception.BadRequestException;
import id.ac.ukdw.thebooks_transaksi.exception.NotFoundException;
import id.ac.ukdw.thebooks_transaksi.model.DetailKeranjang;
import id.ac.ukdw.thebooks_transaksi.model.Keranjang;
import id.ac.ukdw.thebooks_transaksi.model.Buku;
import id.ac.ukdw.thebooks_transaksi.repository.dao.DetailKeranjangDao;
import id.ac.ukdw.thebooks_transaksi.repository.dao.KeranjangDao;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.valid4j.Validation.validate;

@Service
@RequiredArgsConstructor
public class KeranjangService {

    private final ModelMapper mapper;

    private final KeranjangDao keranjangRepository;
    
    private final DetailKeranjangDao detailKeranjangRepository;

    public KeranjangDto getItemKeranjang(String emailUser){
        Optional<Keranjang> data =  keranjangRepository.findByEmail(emailUser);
        if (data.isPresent()){
            Optional<List<DetailKeranjang>> dataItem = 
                    detailKeranjangRepository.findItemByIdKeranjang(
                            data.get().getIdKeranjang());
            KeranjangDto keranjangDto = mapper.map(data.get(),KeranjangDto.class);
            List<Buku> listBuku = new ArrayList<>();
            for (DetailKeranjang item: dataItem.get()){
                listBuku.add(new Buku(item.getIsbn(), item.getQty()));
            }
            keranjangDto.setItem(listBuku);
            return keranjangDto;
        }
        throw new NotFoundException();
    }

    public String addKeranjang(String isbn,
                                String emailUser){
        validate(!isbn.isEmpty(), new BadRequestException());
        validate(isbn!=null, new BadRequestException());
        validate(!emailUser.isEmpty(), new BadRequestException());
        validate(emailUser!=null, new BadRequestException());

        Optional<Keranjang> data = keranjangRepository.findByEmail(emailUser);
        if (data.isPresent()){
            Optional<DetailKeranjang> detailData = 
                    detailKeranjangRepository.findSpecificItem(isbn,emailUser);
            if(detailData.isPresent()){
                DetailKeranjang item = detailData.get();
                item.setQty(item.getQty()+1);
                if (detailKeranjangRepository.update(item)){
                    return "Item ditambahkan ke Keranjang";
                }
            }else{
                DetailKeranjang item = new DetailKeranjang(data.get(),isbn,1);
                if (detailKeranjangRepository.save(item)){
                    return "Item ditambahkan ke Keranjang";
                }
            }
        }else if (data.isEmpty()){
            Keranjang keranjang = new Keranjang(emailUser);
            if (keranjangRepository.save(keranjang)){
                DetailKeranjang item = new DetailKeranjang(keranjang,isbn,1);
                if (detailKeranjangRepository.save(item)){
                    return "Item ditambahkan ke Keranjang";
                }
            }
        }
        throw new BadRequestException();
    }

    public String deleteKeranjang(String idKeranjang){
        Optional<Keranjang> keranjang = keranjangRepository.findById(idKeranjang);
        if(keranjang.isPresent()){
            if (keranjangRepository.delete(idKeranjang)){
                return "Keranjang Dihapus";
            }
        }
        throw new BadRequestException();
    }
    
    public String deleteItemInKeranjang(String idKeranjang, String isbn){
        Optional<Keranjang> keranjang = keranjangRepository.findById(idKeranjang);
        if(keranjang.isPresent()){
            Optional<DetailKeranjang> itemKeranjang = detailKeranjangRepository.findSpecificItem(isbn,idKeranjang);
            if(itemKeranjang.isPresent()){
                if(detailKeranjangRepository.deleteAnItemFromKeranjang(isbn, idKeranjang)){
                    return "Keranjang terhapus";
                }
            }
        }
        throw new BadRequestException();
    }
    
    public String updateItem(String idKeranjang,
                             String isbn,
                             int jumlah){
        validate(!isbn.isEmpty(), new BadRequestException());
        validate(isbn!=null, new BadRequestException());

        Optional<Keranjang> data = keranjangRepository.findById(idKeranjang);
        if(data.isPresent()){
            Optional<DetailKeranjang> dataItem = detailKeranjangRepository.findSpecificItem(isbn,idKeranjang);
            if (dataItem.isPresent()){
                DetailKeranjang keranjang = dataItem.get();
                keranjang.setQty(jumlah);
                if (detailKeranjangRepository.save(keranjang)){
                    return "Update Berhasil";
                }
            }
        }
        throw new BadRequestException();
    }


}
