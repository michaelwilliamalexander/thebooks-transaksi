package id.ac.ukdw.thebooks_transaksi.model;

import lombok.Data;

@Data
public class Buku {

    private String isbn;

    private int qty;

    public Buku(){}

    public Buku(String isbn,int qty){
        this.isbn = isbn;
        this.qty = qty;
    }
}
