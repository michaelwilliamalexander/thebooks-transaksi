package id.ac.ukdw.thebooks_transaksi.repository;

import id.ac.ukdw.thebooks_transaksi.dto.ItemOrderDto;
import id.ac.ukdw.thebooks_transaksi.model.ItemOrder;
import id.ac.ukdw.thebooks_transaksi.repository.dao.ItemlOrderDao;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
@Transactional
@RequiredArgsConstructor
public class ItemOrderRepository implements ItemlOrderDao {

    private final EntityManager entityManager;

    @Override
    public Optional<ItemOrder> findById(Integer id) {
        String hql ="select itm from ItemOrder itm where itm.idDetailOrder = :id";
        Query query = entityManager.createQuery(hql);
        query.setParameter("id",id);
        List<ItemOrder> result = query.getResultList();
        ItemOrder data = null;
        for (ItemOrder item:result){
            data = item;
        }
        return Optional.ofNullable(data);
    }

    @Override
    public boolean save(ItemOrder itemOrder) {
        String hql = "insert into detail_order(id_order,isbn,qty,harga) values(?,?,?,?)";
        Query query = entityManager.createNativeQuery(hql);
        query.setParameter(1,itemOrder.getOrder().getIdOrder());
        query.setParameter(2, itemOrder.getIsbn());
        query.setParameter(3, itemOrder.getQty());
        query.setParameter(4, itemOrder.getHarga());
        query.executeUpdate();
        return true;
    }

    @Override
    public boolean delete(Integer id) {
        String hql = "delete from ItemOrder itm where itm.idDetailOrder = :id";
        Query query = entityManager.createQuery(hql);
        query.setParameter("id",id);
        query.executeUpdate();
        return true;
    }

    @Override
    public boolean update(ItemOrder itemOrder) {
        entityManager.merge(itemOrder);
        return true;
    }

    @Override
    public Optional<List<ItemOrder>> findByIdOrder(String idTransaksi) {
        String hql = "select itv from ItemOrder itv where itv.order.idOrder = :id";
        Query query = entityManager.createQuery(hql);
        query.setParameter("id",idTransaksi);
        List<ItemOrder> result = query.getResultList();
        return Optional.ofNullable(result);
    }

}
