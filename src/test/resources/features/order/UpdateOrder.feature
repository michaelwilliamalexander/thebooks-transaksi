Feature: Update data Order

  Scenario Outline: Mengupdate data order dan berhasil
    Given Terdapat data-data order
      |idOrder      |email                          |alamat                                     |statusOrder|time                |items                                       |qty    |hargaItems             |hargaTotal |
      |INV/aB13245  |michael.william@ti.ukdw.ac.id  |Jalan merdeka no 35, Yogyakarta, DIY 55152 |true       |2020-05-01 08:00:00 |9781974710041, 9780804171588                |1,1    |100000, 115000         |215000     |
      |INV/Bc3245a  |michael.william@ti.ukdw.ac.id  |Jalan damai no 12, Jakarta, DKI 10150      |false      |2020-05-11 10:00:00 |9781338732870                               |2      |200000                 |400000     |
      |INV/gF4567H  |nathaniel.alvin@ti.ukdw.ac.id  |Jalan Kebun Jeruk 10, Jakarta, DKI 11520   |true       |2020-05-05 13:00:00 |9781974710041                               |1      |100000                 |100000     |
      |INV/cG987Ft  |nathaniel.alvin@ti.ukdw.ac.id  |Jalan Kebun Jeruk 10, Jakarta, DKI 11520   |false      |2020-05-09 15:00:00 |9780804171588, 9781338732870, 9781974710041 |1,1,1  |115000, 200000, 100000 |415000     |
    When user dengan id order <idOrder> ingin mengupdate status order menjadi true
    Then user mendapatkan pesan <pesan> dari update Order dengan request <idOrder>
    Examples:
      |idOrder      |pesan         |
      |INV/Bc3245a  |Order Selesai |
      |INV/cG987Ft  |Order Selesai |


  Scenario Outline: Mengupdate data Order dan Gagal
    Given Terdapat data Order
      |idOrder      |email                          |alamat                                     |statusOrder|time                |items                                       |qty    |hargaItems             |hargaTotal |
      |INV/aB13245  |michael.william@ti.ukdw.ac.id  |Jalan merdeka no 35, Yogyakarta, DIY 55152 |true       |2020-05-01 08:00:00 |9781974710041, 9780804171588                |1,1    |100000, 115000         |215000     |
      |INV/Bc3245a  |michael.william@ti.ukdw.ac.id  |Jalan damai no 12, Jakarta, DKI 10150      |false      |2020-05-11 10:00:00 |9781338732870                               |2      |200000                 |400000     |
      |INV/gF4567H  |nathaniel.alvin@ti.ukdw.ac.id  |Jalan Kebun Jeruk 10, Jakarta, DKI 11520   |true       |2020-05-05 13:00:00 |9781974710041                               |1      |100000                 |100000     |
      |INV/cG987Ft  |nathaniel.alvin@ti.ukdw.ac.id  |Jalan Kebun Jeruk 10, Jakarta, DKI 11520   |false      |2020-05-09 15:00:00 |9780804171588, 9781338732870, 9781974710041 |1,1,1  |115000, 200000, 100000 |415000     |
    When user dengan id order <idOrder> ingin memperbarui status Order menjadi true
    Then user mendapatkan Bad Request dari update Order dengan request <idOrder>
    Examples:
      |idOrder      |
      |INV/Bc3245a  |
      |             |
