package id.ac.ukdw.thebooks_transaksi.dto.request.alamat;

import lombok.Data;

@Data
public class AlamatRequest {
    
    private String alamat;
    
    private String kota;
    
    private String provinsi;
    
    private String kodePos;
    
    private String emailUser;
    
    public AlamatRequest(){}
    
    public AlamatRequest(String alamat,
                         String kota,
                         String provinsi,
                         String kodePos,
                         String emailUser){
        this.alamat = alamat;
        this.kodePos = kodePos;
        this.kota = kota;
        this.provinsi = provinsi;
        this.emailUser = emailUser;
    }
}
