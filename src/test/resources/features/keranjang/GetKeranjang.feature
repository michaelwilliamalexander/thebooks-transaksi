Feature: Mendapatkan data Keranjang

  Scenario Outline: Mendapatkan data semua data keranjang dan ditemukan
    Given List data Keranjang
      |emailUser                      |idKeranjang      |items                          |qty    |
      |nathaniel.alvin@ti.ukdw.ac.id  |aB132Qw          |9781974710041 , 9780804171588  |1, 2   |
      |michael.william@ti.ukdw.ac.id  |qe234QW          |9781338732870                  |1      |
    When user dengan email <email> ingin mendapatkan dengan seluruh data keranjang
    Then user mendapatkan semua data keranjang dengan data request <email>
    Examples:
      |email                          |
      |nathaniel.alvin@ti.ukdw.ac.id  |
      |michael.william@ti.ukdw.ac.id  |

  Scenario Outline: Mendapatkan data semua data keranjang dan tidak ditemukan
    Given List data-data Keranjang
      |emailUser                      |idKeranjang      |items                          |qty    |
      |nathaniel.alvin@ti.ukdw.ac.id  |aB132Qw          |9781974710041 , 9780804171588  |1, 2   |
    When user dengan email <email> ingin mendapatkan seluruh data keranjang
    Then user mendapatkan not found dari seluruh data keranjang dengan data request <email>
    Examples:
      |email                          |
      |michael.william@ti.ukdw.ac.id  |