package id.ac.ukdw.thebooks_transaksi.model;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
@Data
@Table(name = "detail_order")
public class ItemOrder {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_detail_order")
    private Integer idDetailOrder;

    @ManyToOne
    @JoinColumn(name = "id_order")
    @NotNull @NotEmpty
    private Order order;

    @NotNull @NotEmpty
    @Column(name = "isbn")
    private String isbn;

    @NotNull @NotEmpty
    @Column(name = "qty")
    private int qty;

    @NotNull @NotEmpty
    @Column(name = "harga")
    private double harga;

    public ItemOrder(){}

    public ItemOrder(int idDetailOrder, Order order, String isbn, int qty, double harga){
        this.idDetailOrder = idDetailOrder;
        this.order = order;
        this.isbn = isbn;
        this.qty = qty;
        this.harga = harga;
    }

    public ItemOrder(Order order, String isbn, int qty, double harga){
        this.order = order;
        this.isbn = isbn;
        this.qty = qty;
        this.harga = harga;
    }
    
}
