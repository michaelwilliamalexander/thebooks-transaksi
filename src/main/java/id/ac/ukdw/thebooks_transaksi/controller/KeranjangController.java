package id.ac.ukdw.thebooks_transaksi.controller;

import id.ac.ukdw.thebooks_transaksi.exception.BadRequestException;
import id.ac.ukdw.thebooks_transaksi.dto.request.keranjang.KeranjangRequest;
import id.ac.ukdw.thebooks_transaksi.dto.request.keranjang.UpdateKeranjangRequest;
import id.ac.ukdw.thebooks_transaksi.dto.response.ResponseWrapper;
import id.ac.ukdw.thebooks_transaksi.service.KeranjangService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import static id.ac.ukdw.thebooks_transaksi.config.SwaggerConfig.*;
import static org.valid4j.Validation.validate;

@Controller
@Api(tags = keranjangServiceTag)
@RequestMapping("/order/keranjang")
@RequiredArgsConstructor
public class KeranjangController {

    private final KeranjangService service;

    @PostMapping(value = "/",produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Menambahkan item ke keranjang")
    public ResponseEntity<ResponseWrapper> addKeranjang(@RequestBody KeranjangRequest request){
        return ResponseEntity.ok(new ResponseWrapper(service.addKeranjang(
                request.getIsbn(),
                request.getEmailUser())));
    }

    @GetMapping(value = "/")
    @ApiOperation(value = "Mengembalikan data keranjang user", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseWrapper> getAllKeranjang(
            @RequestParam("email") String email){
        return ResponseEntity.ok(new ResponseWrapper(service.getItemKeranjang(email)));
    }

    @DeleteMapping(value = "/")
    @ApiOperation(value = "Menghapus data item keranjang")
    public ResponseEntity<ResponseWrapper> deleteItemKeranjang(
            @RequestParam("id") String idKeranjang,
            @RequestParam("isbn") String isbn){
        return ResponseEntity.ok(new ResponseWrapper(service.deleteItemInKeranjang(idKeranjang, isbn)));
    }

    @DeleteMapping(value = "/{id}")
    @ApiOperation(value = "Menghapus semua data keranjang")
    public ResponseEntity<ResponseWrapper> deleteKeranjang(
            @PathVariable("id") String idKeranjang){
        return ResponseEntity.ok(new ResponseWrapper(service.deleteKeranjang(idKeranjang)));
    }

    @PutMapping(value = "/{id}")
    @ApiOperation(value = "Mengubah jumlah data keranjang")
    public ResponseEntity<ResponseWrapper> updateKeranjang(
            @PathVariable("id") String idKeranjang,
            @RequestBody UpdateKeranjangRequest request){
        return ResponseEntity.ok(new ResponseWrapper(
                service.updateItem(
                        idKeranjang,
                        request.getIsbn(),
                        request.getQty())));
    }
}
