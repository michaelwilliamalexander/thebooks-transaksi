package id.ac.ukdw.thebooks_transaksi.repository;

import id.ac.ukdw.thebooks_transaksi.model.DetailKeranjang;
import id.ac.ukdw.thebooks_transaksi.repository.dao.DetailKeranjangDao;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
@Transactional
@RequiredArgsConstructor
public class KeranjangDetailRepository implements DetailKeranjangDao{

    private final EntityManager entityManager;

    @Override
    public Optional<DetailKeranjang> findById(Integer id) {
        String hql = "select  inv from DetailKeranjang inv where inv.idDetail = :id";
        Query query = entityManager.createQuery(hql);
        query.setParameter("id",id);
        List<DetailKeranjang> data = query.getResultList();
        DetailKeranjang result = null;
        for (DetailKeranjang detailKeranjang:data){
            result = detailKeranjang;
        }
        return Optional.ofNullable(result);
    }

    @Override
    public boolean save(DetailKeranjang itemKeranjang) {
        String hql = "insert into detail_keranjang(id_keranjang,isbn,qty) values (?,?,?)";
        Query query = entityManager.createNativeQuery(hql);
        query.setParameter(1, itemKeranjang.getKeranjang().getIdKeranjang());
        query.setParameter(2, itemKeranjang.getIsbn());
        query.setParameter(3, itemKeranjang.getQty());
        query.executeUpdate();
        return true;
    }

    @Override
    public Optional<List<DetailKeranjang>> findItemByIdKeranjang(String idKeranjang) {
        String hql = "select krg from DetailKeranjang krg where krg.keranjang.idKeranjang = :idKeranjang";
        Query query = entityManager.createQuery(hql);
        query.setParameter("idKeranjang",idKeranjang);
        List<DetailKeranjang> data = query.getResultList();
        return Optional.ofNullable(data);
    }

    @Override
    public boolean deleteAllItemFromKeranjang(String idKeranjang) {
        String hql = "delete from DetailKeranjang inv where inv.keranjang.idKeranjang = :id";
        Query query = entityManager.createQuery(hql);
        query.setParameter("id",idKeranjang);
        query.executeUpdate();
        return true;
    }

    @Override
    public Optional<DetailKeranjang> findSpecificItem(String isbn, String email) {
        String hql = "select dkr from DetailKeranjang dkr where " +
                "dkr.keranjang.emailUser = :email and dkr.isbn = :isbn";
        Query query = entityManager.createQuery(hql);
        query.setParameter("email",email);
        query.setParameter("isbn",isbn);
        List<DetailKeranjang> data = query.getResultList();
        DetailKeranjang result = null;
        for (DetailKeranjang item: data){
            result = item;
        }
        return Optional.ofNullable(result);
    }

    @Override
    public boolean deleteAnItemFromKeranjang(String isbn, String idKeranjang) {
        String hql = "delete from DetailKeranjang inv where " +
                "inv.isbn = :isbn and inv.keranjang.idKeranjang = :idKeranjang";
        Query query = entityManager.createQuery(hql);
        query.setParameter("isbn", isbn);
        query.setParameter("idKeranjang", idKeranjang);
        query.executeUpdate();
        return true;
    }
    
    @Override
    public boolean delete(Integer id){
        String hql = "delete from DetailKeranjang inv where inv.idDetail = :id";
        Query query = entityManager.createQuery(hql);
        query.setParameter("id", id);
        query.executeUpdate();
        return true;
    }
    
    @Override
    public boolean update(DetailKeranjang keranjang) {
        entityManager.merge(keranjang);
        return true;
    }


}
