package id.ac.ukdw.thebooks_transaksi.dto;

import lombok.Data;

import java.util.List;

@Data
public class DetailOrderDto {

    private String idOrder;

    private String alamat;

    private List<BukuDto> buku;

    private boolean diterima;

    private String timeOrder;

    private double hargaTotal;

    public DetailOrderDto(){}

    public DetailOrderDto(String idOrder,
                          String alamat,
                          List<BukuDto> buku,
                          boolean diterima,
                          String timeOrder,
                          double hargaTotal){
        this.idOrder = idOrder;
        this.alamat = alamat;
        this.buku = buku;
        this.diterima = diterima;
        this.timeOrder = timeOrder;
        this.hargaTotal = hargaTotal;
    }

}
