package id.ac.ukdw.thebooks_transaksi.repository.dao;

import id.ac.ukdw.thebooks_transaksi.model.Keranjang;

import java.util.List;
import java.util.Optional;

public interface KeranjangDao extends Dao<Keranjang,String> {

    Optional<Keranjang> findByEmail(String email);
}
