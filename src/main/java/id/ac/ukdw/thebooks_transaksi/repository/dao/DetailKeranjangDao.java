package id.ac.ukdw.thebooks_transaksi.repository.dao;

import id.ac.ukdw.thebooks_transaksi.model.DetailKeranjang;

import java.util.List;
import java.util.Optional;

public interface DetailKeranjangDao extends Dao<DetailKeranjang, Integer>{
    
    public Optional<List<DetailKeranjang>> findItemByIdKeranjang(String idKeranjang);
    
    public boolean deleteAllItemFromKeranjang(String idKeranjang);

    public Optional<DetailKeranjang> findSpecificItem(String isbn, String email);

    public boolean deleteAnItemFromKeranjang(String isbn, String idKeranjang);
}