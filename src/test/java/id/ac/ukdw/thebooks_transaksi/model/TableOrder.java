package id.ac.ukdw.thebooks_transaksi.model;

import lombok.Data;

import java.util.List;

@Data
public class TableOrder {

    private String idOrder;

    private  String email;

    private String alamat;

    private boolean statusOrder;

    private String time;

    private String items;

    private String qty;

    private String hargaItems;

    private double hargaTotal;


}
