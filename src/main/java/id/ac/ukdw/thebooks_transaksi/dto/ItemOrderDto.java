package id.ac.ukdw.thebooks_transaksi.dto;

import id.ac.ukdw.thebooks_transaksi.model.Order;
import lombok.Data;

@Data
public class ItemOrderDto {

    private Integer idDetailOrder;

    private Order order;

    private String isbn;

    private int qty;

    private double harga;

}
