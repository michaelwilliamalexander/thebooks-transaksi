package id.ac.ukdw.thebooks_transaksi.features.alamat.get;

import id.ac.ukdw.thebooks_transaksi.config.ObjectMapping;
import id.ac.ukdw.thebooks_transaksi.controller.AlamatController;
import id.ac.ukdw.thebooks_transaksi.dto.AlamatDto;
import id.ac.ukdw.thebooks_transaksi.model.Alamat;
import id.ac.ukdw.thebooks_transaksi.service.AlamatService;
import io.cucumber.java.Before;
import io.cucumber.java.en.*;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class GetAllAlamatTest {

    @InjectMocks
    private AlamatController controller;

    @Mock
    private AlamatService service;

    @Autowired
    private ModelMapper mapper;

    private MockMvc mockMvc;

    private List<Alamat> dataAlamat;

    private List<AlamatDto> detailAlamat;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
        dataAlamat = new ArrayList<>();
        detailAlamat = new ArrayList<>();
    }

    @Given("^Data list alamat user$")
    public void data(List<Alamat> data) {
        dataAlamat = data;
    }

    @When("^user dengan email (.*) ingin menapilkan semua data alamatnya$")
    public void input(final String email) {
        for (Alamat alamat: dataAlamat){
            if (alamat.getEmailUser().equals(email)){
                detailAlamat.add(mapper.map(alamat, AlamatDto.class));
            }
        }
        if (!detailAlamat.isEmpty()) when(service.getAllAlamatUser(email)).thenReturn(detailAlamat);
    }
    @Then("^user mendapatkan semua data alamat dengan data request (.*)$")
    public void output(final String email) throws Exception{

        mockMvc.perform(MockMvcRequestBuilders.get("/order/alamat/")
                .contentType(MediaType.APPLICATION_JSON)
                .param("email",email))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.data",hasSize(detailAlamat.size())));

        verify(service).getAllAlamatUser(email);
    }
}
