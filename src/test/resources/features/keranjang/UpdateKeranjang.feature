Feature: Mengupdate data Keranjang

  Scenario Outline: Mengupdate data keranjang dan Berhasil
    Given Data keranjang
      |emailUser                      |idKeranjang      |items                          |qty    |
      |nathaniel.alvin@ti.ukdw.ac.id  |aB132Qw          |9781974710041 , 9780804171588  |1, 2   |
      |michael.william@ti.ukdw.ac.id  |qe234QW          |9781338732870                  |1      |
    When user mengupdate data dengan request <idKeranjang> , <isbn> , <qty>
    Then user mendapatkan pesan <pesan> dari mengupdate keranjang dengan request <idKeranjang> , <isbn> , <qty>
    Examples:
      |idKeranjang  |isbn             |qty  |pesan            |
      |aB132Qw      |9781974710041    |2    |Update Berhasil  |
      |qe234QW      |9781338732870    |3    |Update Berhasil  |


  Scenario Outline: Mengupdate data keranjang dan Gagal
    Given Data-data keranjang
      |emailUser                      |idKeranjang      |items                          |qty    |
      |nathaniel.alvin@ti.ukdw.ac.id  |aB132Qw          |9781974710041 , 9780804171588  |1, 2   |
      |michael.william@ti.ukdw.ac.id  |qe234QW          |9781338732870                  |1      |
    When user data keranjang dengan request <idKeranjang> , <isbn> , <qty>
    Then user mendapatkan Bad Request dari mengupdate keranjang dengan data request <idKeranjang> , <isbn> , <qty>
    Examples:
      |idKeranjang  |isbn             |qty  |
      |aB132Qw      |9781974710041    |0    |
      |aB132Qw      |                 |     |