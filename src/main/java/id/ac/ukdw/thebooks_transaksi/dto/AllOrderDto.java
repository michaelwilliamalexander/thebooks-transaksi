package id.ac.ukdw.thebooks_transaksi.dto;

import lombok.Data;

import java.util.List;

@Data
public class AllOrderDto{

    private String idOrder;

    private double hargaTotal;

    private String date;

    private String isbnOfFirstItem;

    private boolean statusDiterima;

    private int qtyOfFirstItem;

    private int qtyOfMoreItem;

    public AllOrderDto(){}

    public AllOrderDto(String idOrder,
                       double hargaTotal,
                       String date,
                       boolean statusDiterima,
                       String isbnOfFirstItem,
                       int qtyOfFirstItem,
                       int qtyOfMoreItem
    ){
        this.idOrder = idOrder;
        this.hargaTotal = hargaTotal;
        this.statusDiterima = statusDiterima;
        this.date = date;
        this.isbnOfFirstItem = isbnOfFirstItem;
        this.qtyOfFirstItem = qtyOfFirstItem;
        this.qtyOfMoreItem = qtyOfMoreItem;
    }
}