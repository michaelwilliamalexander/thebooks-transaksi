package id.ac.ukdw.thebooks_transaksi.dto.request;

import lombok.Data;

import java.util.List;

@Data
public class DataKeranjangResponse {
    
    String idKeranjang;
    
    List<String> items;
    
    List<Integer> qty;
    
}
