package id.ac.ukdw.thebooks_transaksi.features.alamat.get;

import id.ac.ukdw.thebooks_transaksi.controller.AlamatController;
import id.ac.ukdw.thebooks_transaksi.dto.AlamatDto;
import id.ac.ukdw.thebooks_transaksi.exception.NotFoundException;
import id.ac.ukdw.thebooks_transaksi.model.Alamat;
import id.ac.ukdw.thebooks_transaksi.service.AlamatService;
import io.cucumber.java.Before;
import io.cucumber.java.en.*;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class GetDetailAlamatFailTest {

    @InjectMocks
    private AlamatController controller;

    @Mock
    private AlamatService service;

    @Autowired
    private ModelMapper mapper;

    private MockMvc mockMvc;

    private List<Alamat> dataAlamat;

    private AlamatDto detailAlamat;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
        dataAlamat = new ArrayList<>();
    }

    @Given("^Data list Alamat$")
    public void data(List<Alamat> data) {
        dataAlamat = data;
    }

    @When("^user ingin mendapatkan detail email dengan id (.*)$")
    public void input(final int idAlamat) {

        when(service.getAlamat(idAlamat)).thenThrow(new NotFoundException());
    }

    @Then("^user mendapatkan Not Found dari pengambilan detail alamat dengan request id (.*)$")
    public void output(final int idAlamat) throws Exception{

        mockMvc.perform(MockMvcRequestBuilders.get("/order/alamat/{id}",idAlamat)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotFound());

        verify(service).getAlamat(idAlamat);
    }

}
