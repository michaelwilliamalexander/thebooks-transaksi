package id.ac.ukdw.thebooks_transaksi.dto.request.order;

import lombok.Data;

@Data
public class UpdateOrderRequest {

    private String idOrder;

    private boolean statusOrder;

    public UpdateOrderRequest(){}

    public UpdateOrderRequest(String idOrder, boolean statusOrder){
        this.idOrder = idOrder;
        this.statusOrder = statusOrder;
    }
}
