package id.ac.ukdw.thebooks_transaksi.dto.request.alamat;

import lombok.Data;

@Data
public class UpdateAlamatRequest {

    private String alamat;

    private String kota;

    private String provinsi;

    private String kodePos;

    public UpdateAlamatRequest(){}

    public UpdateAlamatRequest(String alamat,
                               String kota,
                               String provinsi,
                               String kodePos){
        this.alamat = alamat;
        this.kodePos = kodePos;
        this.kota = kota;
        this.provinsi = provinsi;
    }
}
