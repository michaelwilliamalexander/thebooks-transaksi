package id.ac.ukdw.thebooks_transaksi.model;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.*;

@Entity
@Data
@Table(name = "alamat")
public class Alamat {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_alamat")
    private int idAlamat;

    @NotNull @NotEmpty
    @Column(name = "alamat")
    private String alamat;

    @NotNull @NotEmpty
    @Column(name="kota")
    private String kota;

    @NotNull @NotEmpty
    @Column(name = "provinsi")
    private String provinsi;

    @NotNull @NotEmpty
    @Column(name = "kode_pos")
    private String kodePos;

    @NotNull @NotEmpty
    @Column(name = "email_user")
    private String emailUser;

    public Alamat(){}

    public Alamat(String alamat,
                  String kota,
                  String provinsi,
                  String kodePos,
                  String emailUser){
        this.alamat = alamat;
        this.kota = kota;
        this.provinsi = provinsi;
        this.kodePos = kodePos;
        this.emailUser = emailUser;
    }

    public Alamat(int id,
                  String alamat,
                  String kota,
                  String provinsi,
                  String kodePos,
                  String emailUser){
        this.idAlamat = id;
        this.alamat = alamat;
        this.kota = kota;
        this.provinsi = provinsi;
        this.kodePos = kodePos;
        this.emailUser = emailUser;
    }
}
