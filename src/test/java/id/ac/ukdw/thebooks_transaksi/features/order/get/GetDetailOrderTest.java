package id.ac.ukdw.thebooks_transaksi.features.order.get;

import id.ac.ukdw.thebooks_transaksi.controller.OrderController;
import id.ac.ukdw.thebooks_transaksi.dto.BukuDto;
import id.ac.ukdw.thebooks_transaksi.dto.DetailOrderDto;
import id.ac.ukdw.thebooks_transaksi.model.ItemOrder;
import id.ac.ukdw.thebooks_transaksi.model.Order;
import id.ac.ukdw.thebooks_transaksi.model.TableOrder;
import id.ac.ukdw.thebooks_transaksi.service.OrderService;
import io.cucumber.java.Before;
import io.cucumber.java.en.*;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class GetDetailOrderTest {

    @InjectMocks
    private OrderController controller;

    @Mock
    private OrderService service;

    private MockMvc mockMvc;

    private List<Order> dataOrder;

    private List<ItemOrder> itemOrder;

    private List<BukuDto> dataBuku;

    private DetailOrderDto result;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
        dataOrder = new ArrayList<>();
        itemOrder = new ArrayList<>();
        dataBuku = new ArrayList<>();
    }

    @Given("^Terlist data order$")
    public void data(List<TableOrder> data) {
        for(TableOrder item:data){

            Order order = new Order(
                    item.getIdOrder(),
                    item.getAlamat(),
                    item.getEmail(),
                    item.getTime(),
                    item.isStatusOrder(),
                    item.getHargaTotal());

            List<String> isbn = Arrays.asList(item.getItems().split("\\s*,\\s*"));
            List<String> qty = Arrays.asList(item.getQty().split("\\s*,\\s*"));
            List<String> harga = Arrays.asList(item.getHargaItems().split("\\s*,\\s*"));

            if (isbn.size() == qty.size() && isbn.size() == harga.size()){
                for (int i=0;i<isbn.size();i++){
                    itemOrder.add
                            (new ItemOrder(
                                    order,
                                    isbn.get(i),
                                    Integer.parseInt(qty.get(i)),
                                    Double.parseDouble(harga.get(i))
                                    ));
                }
            }

            dataOrder.add(order);
        }
    }

    @When("^user mengambil detail order dengan id order (.*)$")
    public void input(final String idOrder) {

        for (ItemOrder item: itemOrder){
            if (idOrder.equals(item.getOrder().getIdOrder())){
                dataBuku.add(new BukuDto(item.getIsbn(),item.getQty(),item.getHarga()));
            }
        }

        for (Order item:dataOrder){
            if (idOrder.equals(item.getIdOrder())){
                result = new DetailOrderDto(
                        item.getIdOrder(),
                        item.getAlamat(),
                        dataBuku,
                        item.isStatusOrder(),
                        item.getTimeOrder(),
                        item.getTotalHarga()
                );

            }
        }
        when(service.getOrderDetail(idOrder)).thenReturn(result);
    }


    @Then("^user mendapatkan detail data order dengan id order (.*)$")
    public void output(final String idOrder) throws Exception{

        mockMvc.perform(MockMvcRequestBuilders.get("/order/detail/")
                .param("idOrder",idOrder)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data.idOrder",is(result.getIdOrder())))
                .andExpect(jsonPath("$.data.buku",hasSize(result.getBuku().size())))
                .andExpect(jsonPath("$.data.diterima",is(result.isDiterima())))
                .andExpect(jsonPath("$.data.alamat",is(result.getAlamat())))
                .andExpect(jsonPath("$.data.timeOrder",is(result.getTimeOrder())))
                .andExpect(jsonPath("$.data.hargaTotal",is(result.getHargaTotal())))
                .andDo(print());

        verify(service).getOrderDetail(idOrder);
    }
}
