package id.ac.ukdw.thebooks_transaksi.service;

import id.ac.ukdw.thebooks_transaksi.dto.AllOrderDto;
import id.ac.ukdw.thebooks_transaksi.dto.BukuDto;
import id.ac.ukdw.thebooks_transaksi.dto.DetailOrderDto;
import id.ac.ukdw.thebooks_transaksi.exception.BadRequestException;
import id.ac.ukdw.thebooks_transaksi.exception.NotFoundException;
import id.ac.ukdw.thebooks_transaksi.model.*;
import id.ac.ukdw.thebooks_transaksi.repository.dao.AlamatDao;
import id.ac.ukdw.thebooks_transaksi.repository.dao.OrderDao;
import id.ac.ukdw.thebooks_transaksi.repository.dao.ItemlOrderDao;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.valid4j.Validation.validate;

@Data
@Service
@RequiredArgsConstructor
public class OrderService {

    private final ModelMapper mapper;

    private final AlamatDao alamatRepository;

    private final ItemlOrderDao itemOrderRepository;

    private final OrderDao orderRepository;

    public String addOrder(List<OrderBuku> buku,
                           String emailUser,
                           int idAlamat,
                           String timeOrder){
        validate(!emailUser.isEmpty(), new BadRequestException());
        validate(emailUser!=null, new BadRequestException());
        for (OrderBuku item: buku){
            validate(item.getIsbn() !=null, new BadRequestException());
            validate(!item.getIsbn().isEmpty(), new BadRequestException());
        }

        Optional<Alamat> findAlamat = alamatRepository.findById(idAlamat);
        if (findAlamat.isPresent()){
            List<ItemOrder> dataItem = new ArrayList<>();
            double totalHarga = 0;
            String destination =
                    findAlamat.get().getAlamat()+", " +
                            findAlamat.get().getKota()+", " +
                            findAlamat.get().getProvinsi()+", "+
                            findAlamat.get().getKodePos();
            Order order = new Order(destination,emailUser,timeOrder);

            for (OrderBuku item: buku){
                totalHarga += (item.getHarga()*item.getQty());
                dataItem.add(new ItemOrder(
                        order, item.getIsbn(), item.getQty(), item.getHarga()));
            }
            order.setTotalHarga(totalHarga);
            if (orderRepository.save(order)){
                for (ItemOrder item: dataItem){
                    item.setOrder(order);
                    itemOrderRepository.save(item);
                }
                return order.getIdOrder();
            }
        }
        throw new BadRequestException();
    }

    public List<AllOrderDto> getUnfinishedOrder(String email) {
        Optional<List<Order>> findData = orderRepository.getAllUnfinishedOrders(email);
        if (findData.isPresent()){
            return mappingDataOrder(findData);
        }
        throw new NotFoundException();
    }

    public List<AllOrderDto> getAllOrder(String emailUser){
        Optional<List<Order>> allOrderByEmail = orderRepository.getAllOrdersByEmail(emailUser);
        if(allOrderByEmail.isPresent()){
            return mappingDataOrder(allOrderByEmail);
        }
        throw new NotFoundException();
    }

    private List<AllOrderDto> mappingDataOrder(Optional<List<Order>> findData) {
        List<AllOrderDto> allOrderData = new ArrayList<>();
       if (findData.isPresent()){
           for (Order order:findData.get()){
               Optional<List<ItemOrder>> itemOfOrder = itemOrderRepository.findByIdOrder(order.getIdOrder());
               if (itemOfOrder.isPresent()){
                   int moreItemCounter = 0;
                   for (ItemOrder item:itemOfOrder.get()){
                       moreItemCounter += item.getQty();
                   }
                   moreItemCounter -= itemOfOrder.get().get(0).getQty();

                   allOrderData.add(new AllOrderDto(
                           order.getIdOrder(),
                           order.getTotalHarga(),
                           order.getTimeOrder(),
                           order.isStatusOrder(),
                           itemOfOrder.get().get(0).getIsbn(),
                           itemOfOrder.get().get(0).getQty(),
                           moreItemCounter
                   ));
               }
           }
       }
        return allOrderData;
    }

    public DetailOrderDto getOrderDetail(String idOrder){
        Optional<List<ItemOrder>> findItem = itemOrderRepository.findByIdOrder(idOrder);
        Optional<Order> findOrder = orderRepository.findById(idOrder);

        if (findItem.isPresent() && findOrder.isPresent()){
            List<BukuDto> dataBuku = new ArrayList<>();
            for(ItemOrder item:findItem.get()){
                dataBuku.add(new BukuDto(item.getIsbn(),item.getQty(),item.getHarga()));
            }
            return new DetailOrderDto(
                    findOrder.get().getIdOrder(),
                    findOrder.get().getAlamat(),
                    dataBuku,
                    findOrder.get().isStatusOrder(),
                    findOrder.get().getTimeOrder(),
                    findOrder.get().getTotalHarga());
        }
        throw new NotFoundException();
    }

    public String updateOrder(String idOrder){
        Optional<Order> order = orderRepository.findById(idOrder);
        if (order.isPresent() && !order.get().isStatusOrder()){
            order.get().setStatusOrder(true);
            if (orderRepository.update(order.get())){
                return "Order Selesai";
            }
        }
        throw new BadRequestException();
    }



}
