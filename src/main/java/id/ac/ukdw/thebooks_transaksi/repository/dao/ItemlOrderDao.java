package id.ac.ukdw.thebooks_transaksi.repository.dao;

import id.ac.ukdw.thebooks_transaksi.model.ItemOrder;

import java.util.List;
import java.util.Optional;

public interface ItemlOrderDao extends Dao<ItemOrder,Integer> {

    Optional<List<ItemOrder>> findByIdOrder(String idInvoice);
}
