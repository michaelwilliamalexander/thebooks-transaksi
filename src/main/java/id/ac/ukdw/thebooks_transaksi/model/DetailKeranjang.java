package id.ac.ukdw.thebooks_transaksi.model;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@Entity
@Table(name = "detail_keranjang")
public class DetailKeranjang{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_detail_keranjang")
    private int idDetail;

    @ManyToOne
    @JoinColumn(name = "id_keranjang")
    @NotEmpty @NotNull
    private Keranjang keranjang;

    @NotEmpty @NotNull
    @Column(name = "isbn")
    private String isbn;

    @NotEmpty @NotNull
    @Column(name = "qty")
    private int qty;

    public DetailKeranjang(){}

    public DetailKeranjang(Keranjang keranjang,
                           String isbn,
                           int qty
    ){
        this.keranjang = keranjang;
        this.isbn = isbn;
        this.qty = qty;
    }
}