package id.ac.ukdw.thebooks_transaksi.model;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.UUID;

@Data
@Entity
@Table(name = "keranjang")
public class Keranjang {

    @Id
    @NotNull @NotEmpty
    @Column(name = "id_keranjang")
    private String idKeranjang;

    @NotEmpty @NotNull
    @Column(name = "email_user")
    private String emailUser;

    public Keranjang(){}

    public Keranjang(String idKeranjang, String emailUser){
        this.idKeranjang = idKeranjang;
        this.emailUser = emailUser;
    }

    public Keranjang(String emailUser){
        this.idKeranjang = UUID.randomUUID().toString();
        this.emailUser = emailUser;
    }




}
