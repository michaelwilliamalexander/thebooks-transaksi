package id.ac.ukdw.thebooks_transaksi.features.alamat.add;

import id.ac.ukdw.thebooks_transaksi.config.ObjectMapping;
import id.ac.ukdw.thebooks_transaksi.controller.AlamatController;
import id.ac.ukdw.thebooks_transaksi.dto.request.alamat.AlamatRequest;
import id.ac.ukdw.thebooks_transaksi.exception.BadRequestException;
import id.ac.ukdw.thebooks_transaksi.model.Alamat;
import id.ac.ukdw.thebooks_transaksi.service.AlamatService;
import io.cucumber.java.Before;
import io.cucumber.java.en.*;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class AddAlamatFailTest {

    @InjectMocks
    private AlamatController controller;

    @Mock
    private AlamatService service;

    private MockMvc mockMvc;

    private List<Alamat> dataAlamat;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
        dataAlamat = new ArrayList<>();
    }

    @Given("^Terdapat data alamat$")
    public void data(List<Alamat> data) {
        dataAlamat = data;
    }

    @When("^user dengan (.*) menambahkan alamat dengan data (.*) , (.*) , (.*) , (.*)$")
    public void input(final String email,
                      final String alamat,
                      final String kota,
                      final String provinsi,
                      final String kodePos) {
        when(service.saveAlamat(alamat,kota,provinsi,kodePos,email)).thenThrow(new BadRequestException());
    }
    @Then("^user mendapatkan Bad Request Exception dari data request (.*) , (.*) , (.*) , (.*) , (.*)$")
    public void output(final String email,
                       final String alamat,
                       final String kota,
                       final String provinsi,
                       final String kodePos) throws Exception {

        mockMvc.perform(MockMvcRequestBuilders.post("/order/alamat/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(ObjectMapping.asJsonString(
                        new AlamatRequest(
                                alamat,
                                kota,
                                provinsi,
                                kodePos,
                                email))))
                .andExpect(status().isBadRequest());

        verify(service).saveAlamat(
                alamat,
                kota,
                provinsi,
                kodePos,
                email);
    }
}
