Feature: Update Alamat

  Scenario Outline: Memperbarui alamat dan Berhasil
    Given List data Alamat
      |idAlamat|alamat               |kota               |provinsi         |kodePos          |emailUser                       |
      |1       |Jalan Merdeka no 31  |Yogyakarta         |DIY              |55152            |michael.william@ti.ukdw.ac.id   |
      |2       |Jalan Kebun Jeruk 2  |Jakarta            |DKI Jakarta      |11010            |michael.william@ti.ukdw.ac.id   |
      |3       |Jalan Senopati 23    |Yogyakarta         |DIY              |55132            |nathaniel.alvin@ti.ukdw.ac.id   |
    When user dengan idAlamat <idAlamat> mengupdate data dengan data request <alamat> , <kota> , <provinsi> , <kodePos>
    Then user mendapatkan pesan <pesan> dari data update alamat <idAlamat> , <alamat> , <kota> , <provinsi> , <kodePos>
    Examples:
      |idAlamat|alamat               |kota               |provinsi         |kodePos          |pesan                 |
      |3       |Jalan Merdeka 31     |Yogyakarta         |DIY              |55152            |Update Berhasil       |
      |2       |Jalan Senopati 22    |Yogyakarta         |DIY              |55132            |Update Berhasil       |


  Scenario Outline: Memperbarui alamat dan Gagal
    Given List data-data Alamat
      |idAlamat|alamat               |kota               |provinsi         |kodePos          |emailUser                       |
      |1       |Jalan Merdeka no 31  |Yogyakarta         |DIY              |55152            |michael.william@ti.ukdw.ac.id   |
      |2       |Jalan Kebun Jeruk 2  |Jakarta            |DKI Jakarta      |11010            |michael.william@ti.ukdw.ac.id   |
      |3       |Jalan Senopati 23    |Yogyakarta         |DIY              |55132            |nathaniel.alvin@ti.ukdw.ac.id   |
    When user dengan idAlamat <idAlamat> mengupdate alamat dengan data request <alamat> , <kota> , <provinsi> , <kodePos>
    Then user mendapatkan Bad Request dari data update alamat <idAlamat> , <alamat> , <kota> , <provinsi> , <kodePos>
    Examples:
      |idAlamat|alamat               |kota               |provinsi         |kodePos          |
      |4       |Jalan Merdeka 31     |Yogyakarta         |DIY              |55152            |
      |2       |                     |Yogyakarta         |DIY              |55132            |
      |2       |                     |                   |DIY              |55132            |
      |2       |                     |                   |                 |55132            |
      |2       |                     |                   |                 |                 |