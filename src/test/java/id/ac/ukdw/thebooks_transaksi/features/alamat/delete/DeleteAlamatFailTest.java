package id.ac.ukdw.thebooks_transaksi.features.alamat.delete;

import id.ac.ukdw.thebooks_transaksi.controller.AlamatController;
import id.ac.ukdw.thebooks_transaksi.exception.BadRequestException;
import id.ac.ukdw.thebooks_transaksi.model.Alamat;
import id.ac.ukdw.thebooks_transaksi.service.AlamatService;
import io.cucumber.java.Before;
import io.cucumber.java.en.*;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class DeleteAlamatFailTest {

    @InjectMocks
    private AlamatController controller;

    @Mock
    private AlamatService service;

    private MockMvc mockMvc;

    private List<Alamat> dataAlamat;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
        dataAlamat = new ArrayList<>();
    }

    @Given("^Terlist user alamat$")
    public void data(List<Alamat> data) {
        dataAlamat = data;
    }

    @When("^user ingin menghapus data alamat berid (.*)$")
    public void input(final int idAlamat) {
        when(service.deleteAlamat(idAlamat)).thenThrow(new BadRequestException());
    }
    @Then("^user mendapatkan Bad Request dari menghapus alamat dengan id (.*)$")
    public void output(final int idAlamat) throws Exception{

        mockMvc.perform(MockMvcRequestBuilders.delete("/order/alamat/{id}",idAlamat)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andDo(print());

        verify(service).deleteAlamat(idAlamat);
    }

}
