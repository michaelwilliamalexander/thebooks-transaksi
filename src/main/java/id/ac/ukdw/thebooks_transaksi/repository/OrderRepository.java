package id.ac.ukdw.thebooks_transaksi.repository;

import id.ac.ukdw.thebooks_transaksi.model.Order;
import id.ac.ukdw.thebooks_transaksi.repository.dao.OrderDao;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
@Transactional
@RequiredArgsConstructor
public class OrderRepository implements OrderDao {

    private final EntityManager entityManager;

    @Override
    public Optional<Order> findById(String id) {
        String hql = "select  inv from Order inv where inv.idOrder = :order";
        Query query = entityManager.createQuery(hql);
        query.setParameter("order",id);
        List<Order> data = query.getResultList();
        Order result = null;
        for (Order order:data){
            result = order;
        }
        return Optional.ofNullable(result);
    }

    @Override
    public boolean save(Order order) {
        String hql = "insert into data_order(id_order,email_user,alamat,harga_total,status_order,time_order) " +
                "values (?,?,?,?,?,? )";
        Query query = entityManager.createNativeQuery(hql);
        query.setParameter(1,order.getIdOrder());
        query.setParameter(2,order.getEmailUser());
        query.setParameter(3,order.getAlamat());
        query.setParameter(4,order.getTotalHarga());
        query.setParameter(5,order.isStatusOrder());
        query.setParameter(6,order.getTimeOrder());
        query.executeUpdate();
        return true;
    }

    @Override
    public boolean delete(String id) {
        String hql = "delete from Order inv where inv.idOrder = :idOrder";
        Query query = entityManager.createQuery(hql);
        query.setParameter("idOrder",id);
        query.executeUpdate();
        return true;
    }

    @Override
    public boolean update(Order order) {
        entityManager.merge(order);
        return true;
    }

    @Override
    public Optional<List<Order>> getAllOrdersByEmail(String email) {
        String hql = "select inv from Order inv where inv.emailUser = :email";
        Query query = entityManager.createQuery(hql);
        query.setParameter("email",email);
        List<Order> result = query.getResultList();
        return Optional.ofNullable(result);
    }

    @Override
    public Optional<List<Order>> getAllUnfinishedOrders(String email) {
        String hql = "select inv from Order inv where inv.emailUser = :email and inv.statusOrder = false";
        Query query = entityManager.createQuery(hql);
        query.setParameter("email",email);
        List<Order> result = query.getResultList();
        return Optional.ofNullable(result);
    }


}
