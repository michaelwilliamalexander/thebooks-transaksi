Feature: Menambahkan data Keranjang

  Scenario Outline:Menambahkan Data Keranjang dan Berhasil
    Given Terdapat data keranjang
      |idKeranjang    |emailUser                      |items                           |qty        |
      |aB132we        |michael.william@ti.ukdw.ac.id  |9781974710041, 9780804171588    |1 , 1      |
      |de345sa        |nathaniel.alvin@ti.ukdw.ac.id  |9780804171588                   |2          |
    When user dengan email <email> akan menambahkan keranjang dengan buku berisbn <isbn>
    Then user mendapatkan pesan <pesan> dari penambahan keranjang dengan request <email> , <isbn>
    Examples:
    |email                              |isbn           |pesan                         |
    |michael.william@ti.ukdw.ac.id      |9781974710041  |Item ditambahkan ke Keranjang |
    |ananda.kusumawardana@ti.ukdw.ac.id |9781974710041  |Item ditambahkan ke Keranjang |


  Scenario Outline:Menambahkan Data Keranjang dan Gagal
    Given Terdapat data=data keranjang
      |idKeranjang    |emailUser                      |items            |qty        |
      |aB132we        |michael.william@ti.ukdw.ac.id  |9781974710041    |1          |
      |de345sa        |nathaniel.alvin@ti.ukdw.ac.id  |9780804171588    |2          |
    When user dengan email <email> akan menambahkan item keranjang dengan buku berisbn <isbn>
    Then user mendapatkan Bad Request dari penambahan keranjang denga request <email> , <isbn>
    Examples:
      |email                              |isbn           |
      |michael.william@ti.ukdw.ac.id      |               |
      |                                   |               |
      |                                   |de345sa        |