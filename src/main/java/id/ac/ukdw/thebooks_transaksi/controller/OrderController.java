package id.ac.ukdw.thebooks_transaksi.controller;

import id.ac.ukdw.thebooks_transaksi.dto.request.order.OrderRequest;
import id.ac.ukdw.thebooks_transaksi.dto.response.ResponseWrapper;
import id.ac.ukdw.thebooks_transaksi.exception.BadRequestException;
import id.ac.ukdw.thebooks_transaksi.service.OrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import static id.ac.ukdw.thebooks_transaksi.config.SwaggerConfig.*;
import static org.valid4j.Validation.validate;

@Controller
@RequestMapping("/order")
@Api(tags = orderServiceTag)
@RequiredArgsConstructor
public class OrderController {

    private final OrderService service;

    @PostMapping(value = "/",produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Menambahkan data order")
    public ResponseEntity<ResponseWrapper> addOrder(
            @RequestBody OrderRequest request){
        return ResponseEntity.ok(
                new ResponseWrapper(
                        service.addOrder(
                                request.getBuku(),
                                request.getEmail(),
                                request.getIdAlamat(),
                                request.getTimeOrder()
                        )
                )
        );
    }
    
    @GetMapping(value = "/",produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Mendapatkan semua data order")
    public ResponseEntity<ResponseWrapper> getAllDataOrder(@RequestParam("email") String email){
        return ResponseEntity.ok(
                new ResponseWrapper(
                        service.getAllOrder(email)
                )
        );
    }
    
    @GetMapping(value = "/detail/",produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Mendapatkan detail data order")
    public ResponseEntity<ResponseWrapper> getDetailOrder(@RequestParam("idOrder") String idOrder){
        return ResponseEntity.ok(
                new ResponseWrapper(
                        service.getOrderDetail(idOrder)
                )
        );
    }
    
    @GetMapping(value = "/unfinised/", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Menampilkan semua order yang belum selesai")
    public ResponseEntity<ResponseWrapper> getUnfinishedOrder(@RequestParam("email") String email){
        return ResponseEntity.ok(
                new ResponseWrapper(
                        service.getUnfinishedOrder(email)
                )
        );
    }

    @PutMapping(value = "/",produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Memperbarui status order bahwa telah diterima")
    public ResponseEntity<ResponseWrapper> updateOrder(@RequestParam("idOrder") String idOrder){
        return ResponseEntity.ok(
                new ResponseWrapper(
                        service.updateOrder(idOrder)
                )
        );
    }
}
