package id.ac.ukdw.thebooks_transaksi.repository.dao;

import id.ac.ukdw.thebooks_transaksi.model.Alamat;

import java.util.List;
import java.util.Optional;

public interface AlamatDao extends Dao<Alamat,Integer> {
    
    Optional<List<Alamat>> findByEmailUser(String email);
}
