package id.ac.ukdw.thebooks_transaksi.features.keranjang.get;

import id.ac.ukdw.thebooks_transaksi.controller.KeranjangController;
import id.ac.ukdw.thebooks_transaksi.dto.KeranjangDto;
import id.ac.ukdw.thebooks_transaksi.model.Buku;
import id.ac.ukdw.thebooks_transaksi.model.TableKeranjang;
import id.ac.ukdw.thebooks_transaksi.model.DetailKeranjang;
import id.ac.ukdw.thebooks_transaksi.model.Keranjang;
import id.ac.ukdw.thebooks_transaksi.service.KeranjangService;
import io.cucumber.java.Before;
import io.cucumber.java.en.*;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class GetDataKeranjangTest {

    @InjectMocks
    private KeranjangController controller;

    @Mock
    private KeranjangService service;

    private MockMvc mockMvc;

    private Keranjang dataKeranjang;

    private List<DetailKeranjang> dataDetail;

    private KeranjangDto dataResult;

    private List<Buku> buku;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
        dataDetail = new ArrayList<>();
        buku = new ArrayList<>();
        dataResult = new KeranjangDto();
    }

    @Given("^List data Keranjang$")
    public void data(List<TableKeranjang> data) {
        for (TableKeranjang item:data){
            dataKeranjang = new Keranjang(item.getIdKeranjang(),item.getEmailUser());
            List<String> isbn = Arrays.asList(item.getItems().split("\\s*,\\s*"));
            List<String> qty = Arrays.asList(item.getQty().split("\\s*,\\s*"));
            for (int i=0;i<isbn.size();i++){
                dataDetail.add(new DetailKeranjang(dataKeranjang,isbn.get(i),Integer.parseInt(qty.get(i))));
            }
        }
    }

    @When("^user dengan email (.*) ingin mendapatkan dengan seluruh data keranjang$")
    public void input(final String email) {
        for (DetailKeranjang item: dataDetail){
            if (email.equals(item.getKeranjang().getEmailUser())){
                dataResult.setIdKeranjang(item.getKeranjang().getIdKeranjang());
                dataResult.setEmailUser(item.getKeranjang().getEmailUser());
                buku.add(new Buku(item.getIsbn(),item.getQty()));
            }
        }
        dataResult.setItem(buku);
        when(service.getItemKeranjang(email)).thenReturn(dataResult);

    }
    @Then("^user mendapatkan semua data keranjang dengan data request (.*)$")
    public void output(final String email) throws Exception{

        mockMvc.perform(MockMvcRequestBuilders.get("/order/keranjang/")
                .contentType(MediaType.APPLICATION_JSON)
                .param("email",email))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data.emailUser",is(dataResult.getEmailUser())))
                .andExpect(jsonPath("$.data.idKeranjang",is(dataResult.getIdKeranjang())))
                .andExpect(jsonPath("$.data.item",hasSize(dataResult.getItem().size())))
                .andDo(print());

        verify(service).getItemKeranjang(email);
    }
}
