package id.ac.ukdw.thebooks_transaksi.model;

import lombok.Data;

@Data
public class OrderBuku {

    private String isbn;

    private int qty;

    private double harga;

    public OrderBuku(){}

    public OrderBuku(String isbn, int qty, double harga){
        this.isbn = isbn;
        this.qty = qty;
        this.harga = harga;
    }
}
