package id.ac.ukdw.thebooks_transaksi.dto;

import id.ac.ukdw.thebooks_transaksi.model.Alamat;
import lombok.Data;

@Data
public class OrderDto {

    private String idTransaksi;

    private Alamat alamat;
    
    private String emailUser;

    private String time;

    private boolean diterima;
    
}
