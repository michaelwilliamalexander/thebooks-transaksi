package id.ac.ukdw.thebooks_transaksi.dto;

import id.ac.ukdw.thebooks_transaksi.model.Buku;
import lombok.Data;

import java.util.List;

@Data
public class KeranjangDto {

    private String idKeranjang;

    private String emailUser;

    private List<Buku> item;


}
