package id.ac.ukdw.thebooks_transaksi.dto;

import lombok.Data;

@Data
public class BukuDto {

    private String isbn;

    private int qty;

    private double harga;

    public BukuDto(){}

    public BukuDto(String isbn, int qty, double harga){
        this.isbn = isbn;
        this.harga = harga;
        this.qty = qty;
    }
}
