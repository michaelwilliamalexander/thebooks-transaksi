package id.ac.ukdw.thebooks_transaksi.dto.request.keranjang;

import lombok.Data;

@Data
public class UpdateKeranjangRequest {
    
    private String isbn;
    
    private int qty;
    
    public UpdateKeranjangRequest(){}
    
    public UpdateKeranjangRequest(String isbn, int jumlahItem){
        this.isbn = isbn;
        this.qty = jumlahItem;
    }
    
}
