package id.ac.ukdw.thebooks_transaksi.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Tag;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    public static final String alamatServiceTag = "Alamat";

    public static final String orderServiceTag = "Order";

    public static final String keranjangServiceTag = "Keranjang";

    @Bean
    public Docket apiInfo(){
        return  new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("id.ac.ukdw.thebooks_transaksi.controller"))
                .build()
                .apiInfo(metaData())
                .tags(new Tag(alamatServiceTag, "API untuk alamat"))
                .tags(new Tag(orderServiceTag,"API untuk order"))
                .tags(new Tag(keranjangServiceTag,"API untuk keranjang"));
    }

    private ApiInfo metaData(){
        return new ApiInfoBuilder()
                .title("The Books")
                .description("Dokumentasi API transaksi service")
                .build();
    }
}
