package id.ac.ukdw.thebooks_transaksi.features.order.add;

import id.ac.ukdw.thebooks_transaksi.config.ObjectMapping;
import id.ac.ukdw.thebooks_transaksi.controller.OrderController;
import id.ac.ukdw.thebooks_transaksi.dto.request.order.OrderRequest;
import id.ac.ukdw.thebooks_transaksi.exception.BadRequestException;
import id.ac.ukdw.thebooks_transaksi.model.Alamat;
import id.ac.ukdw.thebooks_transaksi.model.OrderBuku;
import id.ac.ukdw.thebooks_transaksi.service.OrderService;
import io.cucumber.java.Before;
import io.cucumber.java.en.*;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class AddOrderFailTest {

    @InjectMocks
    private OrderController controller;

    @Mock
    private OrderService service;

    private MockMvc mockMvc;

    private List<Alamat> listAlamat;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
        listAlamat = new ArrayList<>();
    }

    @Given("^Terdapat data-data alamat user$")
    public void data(List<Alamat> dataAlamat) {
        listAlamat = dataAlamat;
    }


    @When("^user dengan email (.*) menambahkan Order dengan request (.*) , (.*) , (.*) , (.*) , (.*)$")
    public void input(final String email,
                      final int idAlamat,
                      final String items,
                      final String qty,
                      final String timeOrder,
                      final String hargaItems) {
        List<OrderBuku> itemBuku = new ArrayList<>();
        for (Alamat alamat : listAlamat) {
            if (idAlamat == alamat.getIdAlamat()) {
                if (!items.isEmpty() && !qty.isEmpty() && !hargaItems.isEmpty()){
                    List<String> isbn = Arrays.asList(items.split("\\s*,\\s*"));
                    List<String> jumlah = Arrays.asList(qty.split("\\s*,\\s*"));
                    List<String> harga = Arrays.asList(hargaItems.split("\\s*,\\s*"));
                    if (isbn.size() == harga.size() && isbn.size() == jumlah.size()) {
                        for (int i = 0; i < isbn.size(); i++) {
                            itemBuku.add(new OrderBuku(
                                    isbn.get(i),
                                    Integer.parseInt(jumlah.get(i)),
                                    Double.parseDouble(harga.get(i))
                            ));
                        }
                    }
                }
            }
        }
        when(service.addOrder(itemBuku,email,idAlamat,timeOrder)).thenThrow(new BadRequestException());
    }
    @Then("^user mendapatkan Bad Request dengan menambahkan Order dengan data request (.*) , (.*) ,  (.*) , (.*) , (.*) , (.*)$")
    public void output(final String email,
                       final int idAlamat,
                       final String items,
                       final String qty,
                       final String timeOrder,
                       final String hargaItems)throws Exception {
        List<OrderBuku> itemBuku = new ArrayList<>();

        if (!items.isEmpty() && !qty.isEmpty() && !hargaItems.isEmpty()){
            List<String> isbn = Arrays.asList(items.split("\\s*,\\s*"));
            List<String> jumlah = Arrays.asList(qty.split("\\s*,\\s*"));
            List<String> harga = Arrays.asList(hargaItems.split("\\s*,\\s*"));

            if (isbn.size() == harga.size() && isbn.size() == jumlah.size()) {
                for (int i = 0; i < isbn.size(); i++) {
                    itemBuku.add(new OrderBuku(
                            isbn.get(i),
                            Integer.parseInt(jumlah.get(i)),
                            Double.parseDouble(harga.get(i))
                    ));
                }
            }
        }

        mockMvc.perform(MockMvcRequestBuilders.post("/order/")
                .content(ObjectMapping.asJsonString(new OrderRequest(
                        itemBuku,
                        email,
                        idAlamat,
                        timeOrder)))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andDo(print());


        verify(service).addOrder(itemBuku,email,idAlamat,timeOrder);
    }
}
