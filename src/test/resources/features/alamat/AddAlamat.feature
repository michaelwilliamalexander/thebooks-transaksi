Feature: Menambahkan data Alamat

  Scenario Outline: Menambahkan data alamat user dan Berhasil
    Given Terdapat data-data alamat
    |idAlamat|alamat               |kota               |provinsi         |kodePos          |emailUser                       |
    |1       |Jalan Merdeka no 31  |Yogyakarta         |DIY              |55152            |michael.william@ti.ukdw.ac.id   |
    |2       |Jalan Kebun Jeruk 2  |Jakarta            |DKI Jakarta      |11010            |michael.william@ti.ukdw.ac.id   |
    |3       |Jalan Senopati 23    |Yogyakarta         |DIY              |55132            |nathaniel.alvin@ti.ukdw.ac.id   |
    When user dengan <email> menginputkan data alamat <alamat> , <kota> , <provinsi> , <kodePos>
    Then user mendapatkan pesan <pesan> dari data request <email> , <alamat> , <kota> , <provinsi> , <kodePos>
    Examples:
    |email                          |alamat             |kota           |provinsi         |kodePos      |pesan            |
    |nathaniel.alvin@ti.ukdw.ac.id  |Jalan Flamboyan 1  |Jakarta        |DKI Jakarta      |11010        |Save Berhasil    |
    |michael.william@ti.ukdw.ac.id  |Jalan Jeruk 1      |Jakarta        |DKI Jakarta      |11010        |Save Berhasil    |

  Scenario Outline: Menambahkan data alamat user dan Gagal
    Given Terdapat data alamat
      |idAlamat|alamat               |kota               |provinsi         |kodePos          |emailUser                       |
      |1       |Jalan Merdeka no 31  |Yogyakarta         |DIY              |55152            |michael.william@ti.ukdw.ac.id   |
      |2       |Jalan Kebun Jeruk 2  |Jakarta            |DKI Jakarta      |11010            |michael.william@ti.ukdw.ac.id   |
      |3       |Jalan Senopati 23    |Yogyakarta         |DIY              |55132            |nathaniel.alvin@ti.ukdw.ac.id   |
    When user dengan <email> menambahkan alamat dengan data <alamat> , <kota> , <provinsi> , <kodePos>
    Then user mendapatkan Bad Request Exception dari data request <email> , <alamat> , <kota> , <provinsi> , <kodePos>
    Examples:
      |email                          |alamat               |kota           |provinsi         |kodePos      |
      |                               |Jalan Merdeka no 31  |Yogyakarta     |DIY              |55152        |
      |                               |                     |               |                 |             |
      |michael.william@ti.ukdw.ac.id  |                     |               |                 |             |