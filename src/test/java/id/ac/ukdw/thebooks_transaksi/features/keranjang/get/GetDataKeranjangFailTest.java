package id.ac.ukdw.thebooks_transaksi.features.keranjang.get;

import id.ac.ukdw.thebooks_transaksi.controller.KeranjangController;
import id.ac.ukdw.thebooks_transaksi.exception.NotFoundException;
import id.ac.ukdw.thebooks_transaksi.model.TableKeranjang;
import id.ac.ukdw.thebooks_transaksi.model.DetailKeranjang;
import id.ac.ukdw.thebooks_transaksi.model.Keranjang;
import id.ac.ukdw.thebooks_transaksi.service.KeranjangService;
import io.cucumber.java.Before;
import io.cucumber.java.en.*;
import org.mockito.*;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class GetDataKeranjangFailTest {

    @InjectMocks
    private KeranjangController controller;

    @Mock
    private KeranjangService service;

    private MockMvc mockMvc;

    private Keranjang dataKeranjang;

    private List<DetailKeranjang> dataDetail;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
        dataDetail = new ArrayList<>();
    }

    @Given("^List data-data Keranjang$")
    public void data(List<TableKeranjang> data) {
        for (TableKeranjang item:data){
            dataKeranjang = new Keranjang(item.getIdKeranjang(),item.getEmailUser());
            List<String> isbn = Arrays.asList(item.getItems().split("\\s*,\\s*"));
            List<String> qty = Arrays.asList(item.getQty().split("\\s*,\\s*"));
            if (isbn.size() == qty.size()){
                for (int i=0;i<isbn.size();i++){
                    dataDetail.add(new DetailKeranjang(dataKeranjang,isbn.get(i),Integer.parseInt(qty.get(i))));
                }
            }
        }
    }

    @When("^user dengan email (.*) ingin mendapatkan seluruh data keranjang$")
    public void input(final String email) {
        when(service.getItemKeranjang(email)).thenThrow(new NotFoundException());
    }


    @Then("^user mendapatkan not found dari seluruh data keranjang dengan data request (.*)$")
    public void output(final String email) throws  Exception{
        mockMvc.perform(MockMvcRequestBuilders.get("/order/keranjang/")
                .contentType(MediaType.APPLICATION_JSON)
                .param("email",email))
                .andExpect(status().isNotFound())
                .andDo(print());

        verify(service).getItemKeranjang(email);
    }
}
