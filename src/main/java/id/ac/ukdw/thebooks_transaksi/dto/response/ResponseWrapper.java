package id.ac.ukdw.thebooks_transaksi.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

@JsonInclude(NON_NULL)
@Data
public class ResponseWrapper {
    private Object data;

    public ResponseWrapper() {
    }

    /**
     * @param data
     */
    public ResponseWrapper(Object data) {
        super();
        this.data = data;
    }
}
