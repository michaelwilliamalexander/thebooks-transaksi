Feature: Get Data Alamat

  Scenario Outline: Mendapatkan data detail alamat user dan ditemukan
    Given Data List Alamat
      |idAlamat|alamat               |kota               |provinsi         |kodePos          |emailUser                       |
      |1       |Jalan Merdeka no 31  |Yogyakarta         |DIY              |55152            |michael.william@ti.ukdw.ac.id   |
      |2       |Jalan Kebun Jeruk 2  |Jakarta            |DKI Jakarta      |11010            |michael.william@ti.ukdw.ac.id   |
      |3       |Jalan Senopati 23    |Yogyakarta         |DIY              |55132            |nathaniel.alvin@ti.ukdw.ac.id   |
    When user ingin mendapatkan detail email dengan idAlamat <idAlamat>
    Then user mendapatkan detail alamat dari request id <idAlamat>
    Examples:
    |idAlamat   |
    |1          |
    |2          |

  Scenario Outline: Mendapatkan data detail alamat user dan tidak ditemukan
    Given Data list Alamat
      |idAlamat|alamat               |kota               |provinsi         |kodePos          |emailUser                       |
      |1       |Jalan Merdeka no 31  |Yogyakarta         |DIY              |55152            |michael.william@ti.ukdw.ac.id   |
      |2       |Jalan Kebun Jeruk 2  |Jakarta            |DKI Jakarta      |11010            |michael.william@ti.ukdw.ac.id   |
      |3       |Jalan Senopati 23    |Yogyakarta         |DIY              |55132            |nathaniel.alvin@ti.ukdw.ac.id   |
    When user ingin mendapatkan detail email dengan id <idAlamat>
    Then user mendapatkan Not Found dari pengambilan detail alamat dengan request id <idAlamat>
    Examples:
      |idAlamat   |
      |4          |
      |5          |

  Scenario Outline: Mendapatkan semua data alamat user dan ditemukan
    Given Data list alamat user
      |idAlamat|alamat               |kota               |provinsi         |kodePos          |emailUser                       |
      |1       |Jalan Merdeka no 31  |Yogyakarta         |DIY              |55152            |michael.william@ti.ukdw.ac.id   |
      |2       |Jalan Kebun Jeruk 2  |Jakarta            |DKI Jakarta      |11010            |michael.william@ti.ukdw.ac.id   |
      |3       |Jalan Senopati 23    |Yogyakarta         |DIY              |55132            |nathaniel.alvin@ti.ukdw.ac.id   |
    When user dengan email <email> ingin menapilkan semua data alamatnya
    Then user mendapatkan semua data alamat dengan data request <email>
    Examples:
      |email                           |
      |michael.william@ti.ukdw.ac.id   |
      |nathaniel.alvin@ti.ukdw.ac.id   |


  Scenario Outline: Mendapatkan semua data alama user dan tidak ditemukan
    Given Data-data list alamat user
      |idAlamat|alamat               |kota               |provinsi         |kodePos          |emailUser                       |
      |1       |Jalan Merdeka no 31  |Yogyakarta         |DIY              |55152            |michael.william@ti.ukdw.ac.id   |
      |2       |Jalan Kebun Jeruk 2  |Jakarta            |DKI Jakarta      |11010            |michael.william@ti.ukdw.ac.id   |
      |3       |Jalan Senopati 23    |Yogyakarta         |DIY              |55132            |nathaniel.alvin@ti.ukdw.ac.id   |
    When user dengan email <email> akan menapilkan semua data alamatnya
    Then user mendapatkan pesan Not Found dari pengambilan data alamat berequest <email>
    Examples:
      |email                                |
      |ananada.kusumawardana@ti.ukdw.ac.id  |