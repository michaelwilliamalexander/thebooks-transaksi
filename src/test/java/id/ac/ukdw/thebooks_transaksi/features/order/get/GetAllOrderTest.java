package id.ac.ukdw.thebooks_transaksi.features.order.get;

import id.ac.ukdw.thebooks_transaksi.controller.OrderController;
import id.ac.ukdw.thebooks_transaksi.dto.AllOrderDto;
import id.ac.ukdw.thebooks_transaksi.model.ItemOrder;
import id.ac.ukdw.thebooks_transaksi.model.Order;
import id.ac.ukdw.thebooks_transaksi.model.TableOrder;
import id.ac.ukdw.thebooks_transaksi.service.OrderService;
import io.cucumber.java.Before;
import io.cucumber.java.en.*;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class GetAllOrderTest {

    @InjectMocks
    private OrderController controller;

    @Mock
    private OrderService service;

    private MockMvc mockMvc;

    private List<Order> dataOrder;

    private List<ItemOrder> itemOrder;

    private List<AllOrderDto> result;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
        dataOrder = new ArrayList<>();
        itemOrder = new ArrayList<>();
        result = new ArrayList<>();
    }

    @Given("^List data Order$")
    public void data(List<TableOrder> data) {
        for(TableOrder item:data){

            Order order = new Order(
                    item.getIdOrder(),
                    item.getAlamat(),
                    item.getEmail(),
                    item.getTime(),
                    item.isStatusOrder(),
                    item.getHargaTotal());

            List<String> isbn = Arrays.asList(item.getItems().split("\\s*,\\s*"));
            List<String> qty = Arrays.asList(item.getQty().split("\\s*,\\s*"));
            List<String> harga = Arrays.asList(item.getHargaItems().split("\\s*,\\s*"));

            if (isbn.size() == qty.size() && isbn.size() == harga.size()){
                for (int i=0;i<isbn.size();i++){
                    itemOrder.add
                            (new ItemOrder(
                                    order,
                                    isbn.get(i),
                                    Integer.parseInt(qty.get(i)),
                                    Double.parseDouble(harga.get(i))
                            ));
                }
            }

            dataOrder.add(order);
        }
    }

    @When("^user dengan email (.*) ingin mendapatkan semua data (.*) , (.*) , (.*) , (.*) , (.*) , (.*) , (.*)$")
    public void input(final String email,
                      final String idOrder,
                      final String diterima,
                      final String hargaTotal,
                      final String firstItemQty,
                      final String firstItemIsbn,
                      final String moreItemQty,
                      final String time) {
        List<String> resultIdOrder = Arrays.asList(idOrder.split("\\s*,\\s*"));
        List<String> resultStatusDiterima = Arrays.asList(diterima.split("\\s*,\\s*"));
        List<String> resultHargaTotal   = Arrays.asList(hargaTotal.split("\\s*,\\s*"));
        List<String> resultFirstItemQty = Arrays.asList(firstItemQty.split("\\s*,\\s*"));
        List<String> resultFirstIsbn = Arrays.asList(firstItemIsbn.split("\\s*,\\s*"));
        List<String> resultMoreItemQty = Arrays.asList(moreItemQty.split("\\s*,\\s*"));
        List<String> resultTime = Arrays.asList(time.split("\\s*,\\s*"));

        for (Order item: dataOrder){
            if (email.equals(item.getEmailUser())){
                for (int i=0;i<resultIdOrder.size();i++){
                    if (item.getIdOrder().equals(resultIdOrder.get(i))){
                        result.add(new AllOrderDto(
                                resultIdOrder.get(i),
                                item.getTotalHarga(),
                                resultTime.get(i),
                                Boolean.parseBoolean(resultStatusDiterima.get(i)),
                                resultFirstIsbn.get(i),
                                Integer.parseInt(resultFirstItemQty.get(i)),
                                Integer.parseInt(resultMoreItemQty.get(i))
                        ));
                    }
                }
            }
        }
        when(service.getAllOrder(email)).thenReturn(result);
    }
    @Then("^user mendapatkan semua data order dengan data request (.*)$")
    public void output(final String email) throws Exception{

        mockMvc.perform(MockMvcRequestBuilders.get("/order/")
                .contentType(MediaType.APPLICATION_JSON)
                .param("email",email))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data",hasSize(result.size())))
                .andDo(print());

        verify(service).getAllOrder(email);
    }
}
