Feature: Mendapatkan data-data Order

  Scenario Outline: Mendapatkan semua data order user dan ditemukan
    Given List data Order
      |idOrder      |email                          |alamat                                     |statusOrder|time                |items                                       |qty    |hargaItems             |hargaTotal |
      |INV/aB13245  |michael.william@ti.ukdw.ac.id  |Jalan merdeka no 35, Yogyakarta, DIY 55152 |true       |2020-05-01 08:00:00 |9781974710041, 9780804171588                |1,1    |100000, 115000         |215000     |
      |INV/Bc3245a  |michael.william@ti.ukdw.ac.id  |Jalan damai no 12, Jakarta, DKI 10150      |false      |2020-05-11 10:00:00 |9781338732870                               |2      |200000                 |400000     |
      |INV/gF4567H  |nathaniel.alvin@ti.ukdw.ac.id  |Jalan Kebun Jeruk 10, Jakarta, DKI 11520   |true       |2020-05-05 13:00:00 |9781974710041                               |1      |100000                 |100000     |
      |INV/cG987Ft  |nathaniel.alvin@ti.ukdw.ac.id  |Jalan Kebun Jeruk 10, Jakarta, DKI 11520   |false      |2020-05-09 15:00:00 |9780804171588, 9781338732870, 9781974710041 |1,1,1  |115000, 200000, 100000 |415000     |
    When user dengan email <email> ingin mendapatkan semua data <idOrder> , <statusOrder> , <hargaTotal> , <firstItemQty> , <firstItemIsbn> , <moreItemQty> , <time>
    Then user mendapatkan semua data order dengan data request <email>
    Examples:
      |email                          |idOrder                  |statusOrder  |hargaTotal       |firstItemQty     |firstItemIsbn                 |moreItemQty   |time                                     |
      |michael.william@ti.ukdw.ac.id  |INV/aB13245, INV/Bc3245a |true,false   |215000, 400000   |2, 1             |9781974710041, 9781338732870  |1, 0          |2020-05-01 08:00:00, 2020-05-11 10:00:00 |
      |nathaniel.alvin@ti.ukdw.ac.id  |INV/gF4567H, INV/cG987Ft |true,false   |100000, 415000   |0, 2             |9781974710041, 9780804171588  |0, 2          |2020-05-05 13:00:00, 2020-05-09 15:00:00 |

  Scenario Outline: Mendapatkan semua data order user dan tidak ditemukan
    Given List data-data Order
      |idOrder      |email                          |alamat                                     |statusOrder|time                |items                                       |qty    |hargaItems             |hargaTotal |
      |INV/aB13245  |michael.william@ti.ukdw.ac.id  |Jalan merdeka no 35, Yogyakarta, DIY 55152 |true       |2020-05-01 08:00:00 |9781974710041, 9780804171588                |1,1    |100000, 115000         |215000     |
      |INV/Bc3245a  |michael.william@ti.ukdw.ac.id  |Jalan damai no 12, Jakarta, DKI 10150      |false      |2020-05-11 10:00:00 |9781338732870                               |2      |200000                 |400000     |
    When user dengan email <email> ingin mendapatkan list semua data order
    Then user mendapatkan Not Found dari pengambilan semua data order dengan data request <email>
    Examples:
      |email                          |
      |nathaniel.alvin@ti.ukdw.ac.id  |

  Scenario Outline:Menampilkan detail order dari user dan ditemukan
    Given Terlist data order
      |idOrder      |email                          |alamat                                     |statusOrder|time                |items                                       |qty    |hargaItems             |hargaTotal |
      |INV/aB13245  |michael.william@ti.ukdw.ac.id  |Jalan merdeka no 35, Yogyakarta, DIY 55152 |true       |2020-05-01 08:00:00 |9781974710041, 9780804171588                |1,1    |100000, 115000         |215000     |
      |INV/Bc3245a  |michael.william@ti.ukdw.ac.id  |Jalan damai no 12, Jakarta, DKI 10150      |false      |2020-05-11 10:00:00 |9781338732870                               |2      |200000                 |400000     |
      |INV/gF4567H  |nathaniel.alvin@ti.ukdw.ac.id  |Jalan Kebun Jeruk 10, Jakarta, DKI 11520   |false      |2020-05-05 13:00:00 |9781974710041                               |1      |100000                 |100000     |
      |INV/cG987Ft  |nathaniel.alvin@ti.ukdw.ac.id  |Jalan Kebun Jeruk 10, Jakarta, DKI 11520   |false      |2020-05-09 15:00:00 |9780804171588, 9781338732870, 9781974710041 |1,1,1  |115000, 200000, 100000 |415000     |
    When user mengambil detail order dengan id order <idOrder>
    Then user mendapatkan detail data order dengan id order <idOrder>
    Examples:
      |idOrder      |
      |INV/aB13245  |
      |INV/gF4567H  |

  Scenario Outline:Menampilkan detail order dari user dan tidak ditemukan
    Given Terlist data-data order
      |idOrder      |email                          |alamat                                     |statusOrder|time                |items                                       |qty    |hargaItems             |hargaTotal |
      |INV/aB13245  |michael.william@ti.ukdw.ac.id  |Jalan merdeka no 35, Yogyakarta, DIY 55152 |true       |2020-05-01 08:00:00 |9781974710041, 9780804171588                |1,1    |100000, 115000         |215000     |
      |INV/Bc3245a  |michael.william@ti.ukdw.ac.id  |Jalan damai no 12, Jakarta, DKI 10150      |false      |2020-05-11 10:00:00 |9781338732870                               |2      |200000                 |400000     |
    When user ingin mengambil detail order dengan id <idOrder>
    Then user mendapatkan Not Found dari detail data order dengan id order <idOrder>
    Examples:
      |idOrder      |
      |INV/gF4567H  |

  Scenario Outline: Menampillkan data order unfinished dan ditemukan
    Given Semua data order
      |idOrder      |email                          |alamat                                     |statusOrder|time                |items                                       |qty    |hargaItems             |hargaTotal |
      |INV/aB13245  |michael.william@ti.ukdw.ac.id  |Jalan merdeka no 35, Yogyakarta, DIY 55152 |true       |2020-05-01 08:00:00 |9781974710041, 9780804171588                |1,1    |100000, 115000         |215000     |
      |INV/Bc3245a  |michael.william@ti.ukdw.ac.id  |Jalan damai no 12, Jakarta, DKI 10150      |false      |2020-05-11 10:00:00 |9781338732870                               |2      |200000                 |400000     |
      |INV/gF4567H  |nathaniel.alvin@ti.ukdw.ac.id  |Jalan Kebun Jeruk 10, Jakarta, DKI 11520   |false      |2020-05-05 13:00:00 |9781974710041                               |1      |100000                 |100000     |
      |INV/cG987Ft  |nathaniel.alvin@ti.ukdw.ac.id  |Jalan Kebun Jeruk 10, Jakarta, DKI 11520   |false      |2020-05-09 15:00:00 |9780804171588, 9781338732870, 9781974710041 |1,1,1  |115000, 200000, 100000 |415000     |
    When user dengan email <email> ingin mendapatkan data unfinished order <idOrder> , <statusOrder> , <hargaTotal> , <firstItemQty> , <firstItemIsbn> , <moreItemQty> , <time>
    Then user mendapatkan data unfinished order dengan email <email>
    Examples:
      |email                          |idOrder                  |statusOrder     |hargaTotal       |firstItemQty     |firstItemIsbn                 |moreItemQty   |time                                     |
      |michael.william@ti.ukdw.ac.id  |INV/Bc3245a              |false        |400000           |2                |9781338732870                 |0             |2020-05-11 10:00:00                      |
      |nathaniel.alvin@ti.ukdw.ac.id  |INV/gF4567H, INV/cG987Ft |false,false  |100000, 415000   |1, 1             |9781974710041, 9780804171588  |0,2           |2020-05-05 13:00:00, 2020-05-09 15:00:00 |

  Scenario Outline: Menampillkan data order unfinished dan tidak ditemukan
    Given Semua data-data order
      |idOrder      |email                          |alamat                                     |statusOrder|time                |items                                       |qty    |hargaItems             |hargaTotal |
      |INV/aB13245  |michael.william@ti.ukdw.ac.id  |Jalan merdeka no 35, Yogyakarta, DIY 55152 |true       |2020-05-01 08:00:00 |9781974710041, 9780804171588                |1,1    |100000, 115000         |215000     |
      |INV/Bc3245a  |michael.william@ti.ukdw.ac.id  |Jalan damai no 12, Jakarta, DKI 10150      |false      |2020-05-11 10:00:00 |9781338732870                               |2      |200000                 |400000     |
      |INV/gF4567H  |nathaniel.alvin@ti.ukdw.ac.id  |Jalan Kebun Jeruk 10, Jakarta, DKI 11520   |false      |2020-05-05 13:00:00 |9781974710041                               |1      |100000                 |100000     |
      |INV/cG987Ft  |nathaniel.alvin@ti.ukdw.ac.id  |Jalan Kebun Jeruk 10, Jakarta, DKI 11520   |false      |2020-05-09 15:00:00 |9780804171588, 9781338732870, 9781974710041 |1,1,1  |115000, 200000, 100000 |415000     |
    When user dengan email <email> akan mendapatkan data unfinished order
    Then user mendapatkan data unfinished order dengan request email <email>
    Examples:
      |email                          |
      |michael.william@ti.ukdw.ac.id  |