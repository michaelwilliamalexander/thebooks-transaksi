package id.ac.ukdw.thebooks_transaksi.features.keranjang.delete;

import id.ac.ukdw.thebooks_transaksi.controller.KeranjangController;
import id.ac.ukdw.thebooks_transaksi.model.TableKeranjang;
import id.ac.ukdw.thebooks_transaksi.model.DetailKeranjang;
import id.ac.ukdw.thebooks_transaksi.model.Keranjang;
import id.ac.ukdw.thebooks_transaksi.service.KeranjangService;
import io.cucumber.java.Before;
import io.cucumber.java.en.*;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class DeleteAllKeranjangTest {

    @InjectMocks
    private KeranjangController controller;

    @Mock
    private KeranjangService service;

    private MockMvc mockMvc;

    private Keranjang dataKeranjang;

    private List<DetailKeranjang> dataDetail;


    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
        dataDetail = new ArrayList<>();
    }

    @Given("^data list keranjang$")
    public void data(List<TableKeranjang> data) {
        for (TableKeranjang item:data){
            dataKeranjang = new Keranjang(item.getIdKeranjang(),item.getEmailUser());
            List<String> isbn = Arrays.asList(item.getItems().split("\\s*,\\s*"));
            List<String> qty = Arrays.asList(item.getQty().split("\\s*,\\s*"));
            for (int i=0;i<isbn.size();i++){
                dataDetail.add(new DetailKeranjang(dataKeranjang,isbn.get(i),Integer.parseInt(qty.get(i))));
            }
        }
    }

    @When("^user ingin menghapus semua data keranjang dengan id (.*)")
    public void input(final String idKeranjang) {
        for (DetailKeranjang item:dataDetail){
            if (idKeranjang.equals(item.getKeranjang().getIdKeranjang())){
                when(service.deleteKeranjang(idKeranjang)).thenReturn("Keranjang terhapus");
            }
        }
    }
    @Then("^user mendapatkan pesan (.*) dari penghapusan keranjang dengan request (.*)$")
    public void output(final String pesan,
                       final String idKeranjang) throws Exception{

        mockMvc.perform(MockMvcRequestBuilders.delete("/order/keranjang/{id}",idKeranjang)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data",is(pesan)))
                .andDo(print());

        verify(service).deleteKeranjang(idKeranjang);
    }
}
