package id.ac.ukdw.thebooks_transaksi.features.alamat.update;

import id.ac.ukdw.thebooks_transaksi.config.ObjectMapping;
import id.ac.ukdw.thebooks_transaksi.controller.AlamatController;
import id.ac.ukdw.thebooks_transaksi.dto.request.alamat.UpdateAlamatRequest;
import id.ac.ukdw.thebooks_transaksi.exception.BadRequestException;
import id.ac.ukdw.thebooks_transaksi.model.Alamat;
import id.ac.ukdw.thebooks_transaksi.service.AlamatService;
import io.cucumber.java.Before;
import io.cucumber.java.en.*;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class UpdateAlamatFailTest {

    @InjectMocks
    private AlamatController controller;

    @Mock
    private AlamatService service;

    @Autowired
    private ModelMapper mapper;

    private MockMvc mockMvc;

    private List<Alamat> dataAlamat;


    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
        dataAlamat = new ArrayList<>();
    }

    @Given("^List data-data Alamat$")
    public void data(List<Alamat> data) {
     dataAlamat = data;
    }

    @When("^user dengan idAlamat (.*) mengupdate alamat dengan data request (.*) , (.*) , (.*) , (.*)$")
    public void input(final int idAlamat,
                      final String alamat,
                      final String kota,
                      final String provinsi,
                      final String kodePos){
        when(service.updateAlamat(
                idAlamat,
                alamat,
                kota,
                provinsi,
                kodePos)).thenThrow(new BadRequestException());
    }
    @Then("^user mendapatkan Bad Request dari data update alamat (.*) , (.*) , (.*) , (.*) , (.*)$")
    public void output(final int idAlamat,
                       final String alamat,
                       final String kota,
                       final String provinsi,
                       final String kodePos) throws Exception{
        mockMvc.perform(MockMvcRequestBuilders.put("/order/alamat/{id}",idAlamat)
                .content(ObjectMapping.asJsonString(
                        new UpdateAlamatRequest(
                                alamat,
                                kota,
                                provinsi,
                                kodePos
                        ))
                ).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

        verify(service).updateAlamat(
                idAlamat,
                alamat,
                kota,
                provinsi,
                kodePos
        );
    }
}
