package id.ac.ukdw.thebooks_transaksi.features.keranjang.update;

import id.ac.ukdw.thebooks_transaksi.config.ObjectMapping;
import id.ac.ukdw.thebooks_transaksi.controller.KeranjangController;
import id.ac.ukdw.thebooks_transaksi.dto.request.keranjang.UpdateKeranjangRequest;
import id.ac.ukdw.thebooks_transaksi.exception.BadRequestException;
import id.ac.ukdw.thebooks_transaksi.model.TableKeranjang;
import id.ac.ukdw.thebooks_transaksi.model.DetailKeranjang;
import id.ac.ukdw.thebooks_transaksi.model.Keranjang;
import id.ac.ukdw.thebooks_transaksi.service.KeranjangService;
import io.cucumber.java.Before;
import io.cucumber.java.en.*;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class UpdateKeranjangFailTest {

    @InjectMocks
    private KeranjangController controller;

    @Mock
    private KeranjangService service;

    private MockMvc mockMvc;

    private Keranjang dataKeranjang;

    private List<DetailKeranjang> dataDetail;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
        dataDetail = new ArrayList<>();
    }

    @Given("^Data-data keranjang$")
    public void data(List<TableKeranjang> data) {
        for (TableKeranjang item:data){
            dataKeranjang = new Keranjang(item.getIdKeranjang(),item.getEmailUser());
            List<String> isbn = Arrays.asList(item.getItems().split("\\s*,\\s*"));
            List<String> qty = Arrays.asList(item.getQty().split("\\s*,\\s*"));
            for (int i=0;i<isbn.size();i++){
                dataDetail.add(new DetailKeranjang(dataKeranjang,isbn.get(i),Integer.parseInt(qty.get(i))));
            }
        }
    }

    @When("^user data keranjang dengan request (.*) , (.*) , (.*)$")
    public void input(final String idKeranjang,
                      final String isbn,
                      final int qty) {
        when(service.updateItem(idKeranjang,isbn,qty)).thenThrow(new BadRequestException());
    }


    @Then("^user mendapatkan Bad Request dari mengupdate keranjang dengan data request (.*) , (.*) , (.*)$")
    public void output(final String idKeranjang,
                       final String isbn,
                       final int qty) throws Exception{

        mockMvc.perform(MockMvcRequestBuilders.put("/order/keranjang/{id}",idKeranjang)
                .contentType(MediaType.APPLICATION_JSON)
                .content(ObjectMapping.asJsonString(new UpdateKeranjangRequest(isbn,qty))))
                .andExpect(status().isBadRequest())
                .andDo(print());

        verify(service).updateItem(idKeranjang,isbn,qty);
    }
}
